// OneIRA Project-specific JS
$(document).on('bodyContLoaded', function(){
 
function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

    $('.orangeClr').css('background-color','#fff6d6'); 
	$('.bdz').attr('style', 'border: 0px !important'); 
	$('#dateandTime').attr("style", "margin-top: -6px !important; font-family: arial !important" ); 
	$('#fSource').attr("style", "margin-top: -6px !important; font-family: arial !important" ); 
	
	$('#dateandTime3').attr("style", "margin-top: -6px !important; font-family: arial !important" ); 
	$('#dateandTime2').attr('style', 'margin-top: 0px ;font-family: arial !important'); 
	$('#dateandTime4').attr('style', 'margin-top: -1px ;font-family: arial !important'); 
	
	 	$('#primaryemail').attr("style", "margin-top: -22px !important");
		$('#releaseto_fullfillment').attr("style", "margin-top: 1px !important");	 
	$("#select_signatory").css('width','180px');
	$("#select_signatory").css('height','35px');
	var now = new Date(Date.now());
    var formatted = pad(now.getHours(),2) + ":" + pad(now.getMinutes(),2) + ":" + pad(now.getSeconds(),2);  
    var date = new Date(); 
    
	                          $("#dateTimemsg").click(function(e){  
							  var collapsedexpanded=$(this).find('a').attr('class'); 
						      if(collapsedexpanded=="collapsed"){
							  $("#dateandTime").html(formatted+"&nbsp&nbsp&nbsp"+date.toJSON().slice(0,10).replace(new RegExp("-", 'g'),"/" ).split("/").reverse().join("/")); 
							  }else{
							  $("#dateandTime").html("(from MyTIAA.org)"); 
							  }
						      });
							   
							   $("#dateTimemsg2").click(function(e){  
							  var collapsedexpanded=$(this).find('a').attr('class'); 
						      if(collapsedexpanded=="collapsed"){
							  $("#dateandTime2").html(formatted+"&nbsp&nbsp&nbsp"+date.toJSON().slice(0,10).replace(new RegExp("-", 'g'),"/" ).split("/").reverse().join("/")); 
							  }else{
							  $("#dateandTime2").html("(from MyTIAA.org)"); 
							  }
						      });
							   
							   	   $("#dateTimemsg3").click(function(e){  
							  var collapsedexpanded=$(this).find('a').attr('class'); 
						      if(collapsedexpanded=="collapsed"){
							  $("#dateandTime3").html(formatted+"&nbsp&nbsp&nbsp"+date.toJSON().slice(0,10).replace(new RegExp("-", 'g'),"/" ).split("/").reverse().join("/")); 
							  }else{
							  $("#dateandTime3").html("(from MyTIAA.org)"); 
							  }
						      });
								   
						      $("#dateTimemsg4").click(function(e){  
							  var collapsedexpanded=$(this).find('a').attr('class'); 
						      if(collapsedexpanded=="collapsed"){
							  $("#dateandTime4").html(formatted+"&nbsp&nbsp&nbsp"+date.toJSON().slice(0,10).replace(new RegExp("-", 'g'),"/" ).split("/").reverse().join("/")); 
							  }else{
							  $("#dateandTime4").html("(from MyTIAA.org)"); 
							  }
						      });
							    

    var formstatusVal;
	$(document).on('click', '#fundingformbtn', function() {
		$("#fundformreview2").addClass("closed")
		$("#fundformreview1").removeClass("closed")
	});


	$(document).on('click', '#searchbtn', function() {
		$("#searchkw").html($("#zipcode").val())
		$("#searchresult").removeClass("closed");
	});


	// TPA2Cash or Lump Sum
	$(document).on('click', 'input[id=thirdPartyForms-other]', function() { 
		if ($(this).is(':checked')) {
			$(".otherthirdParty").removeClass('closed');
		} else {
			$(".otherthirdParty").addClass('closed');
		}
	});
	
	// Multi Select
	$.getScript('assets/js/multi_select.js', function(){
		$('.multiSel').multipleSelect();
	});	

	// Dollar formatting
  $(document).on('blur', '.dollarFormat', function() {
    this.value = parseFloat(this.value.replace('$','').replace(/,/g, ""))
                  .toFixed(2)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    this.value = this.value;
  });
  
  // Percentage formatting
  $(document).on('blur', '.percentageFormat', function() {
  	if ($(this).val().indexOf("%") > -1 ) {
    	this.value = this.value.substr(0,this.value.length-1) + '%';
   	} else {
   		this.value = this.value + '%';
   	}
  });

	
	/*==v  Authorization Requirements Flow v==*/
	var authReqModel = {
		'transferTypeVal': '',
		'custAuthMethodVal': ''
	};

	//Update the Forms Handling Summary
	function formsHandlingSummaryUpdate(ind2Update, indVal) {
			var $elem = $('.' + ind2Update),
					$elemNewVal = indVal;
			
			$elem.text(indVal);
	};
	/*==^  Authorization Requirements Flow ^==*/
  
	// Transfer Total Table 
	$(document).on('change', 'input[name=existingInvestment]', function() {
		var $transferTotal = 0;

		$("input[name=existingInvestment]").each(function() {
			var $thisValue = ($(this).val() != '') ? parseFloat($(this).val().replace('$','').replace(',','')) : 0;
    	$transferTotal = parseFloat($transferTotal) + parseFloat($thisValue);

    	var currBal = parseFloat($(this).closest('td').prev('td').text().replace('$','').replace(',','')),
    			percentageOfTotal = $thisValue / currBal * 100;
    	$(this).closest('td').next('td').find('input[name=percentOfTotal]').val(percentageOfTotal.toFixed(2) + '%');
		});
		
		$('#transferTotal').text($transferTotal);
		formatCurrencyText('#transferTotal');
	});
 
  // Format Currency to Text
  function formatCurrencyText(element) {
  	var val = $(element).text();
  	val = parseFloat(val.replace(/,/g, ""))
                  .toFixed(2)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    val = '$' + val;
    $(element).text(val);
  };
  // Format Currency to Input Value
  function formatCurrencyValue(element){
  	var val = $(element).val();
  	val = parseFloat(val.replace(/,/g, ""))
                  .toFixed(2)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    val = '$' + val;
    $(element).val(val);
  };

	// Show Selected Internal Popups
	$(document).on('click','.showSelectIntContracts', function(e) {
		e.preventDefault();
		$(this).closest('.popup').dialog('close');
		$('.selInContracts').removeClass('hidden');
	});

	//Remove row
	$(document).on('click','.delRow', function(e) {
		$(this).closest('tr').remove();
	});
	 
	// START - Forms Validation for Transfer Details
	function requiredFieldFail(element, wrapper) {
		var id = element.attr('id'),
				errorWrapper = wrapper,
		 		elementParent = element.closest('.lblFieldPairV, .lblFieldPair, tr'),
				elementError = $("#"+id+"-err");
		
		// Highlight field and show error
		elementParent.addClass('alertHighlight');
		elementError.removeClass('hidden');
		
		// Show Error Box
		$(errorWrapper).addClass('visible');
		errorBox($(errorWrapper));
	};

	function requiredFieldSuccess(element, wrapper) {
		var id =element.attr('id'),
				errorWrapper = wrapper,
			 	elementParent = element.closest('.lblFieldPairV, .lblFieldPair, tr'),
				elementError = $("#"+id+"-err");

		// Remove highlight from field and show error
		elementParent.removeClass('alertHighlight');
		elementError.addClass('hidden');
		
		// Hide Error Box
		errorBox($(errorWrapper));
	};

	function errorBox(elem) {
		if (elem.find('li:visible').length === 0) {
		 	elem.removeClass('visible');
		} else if (elem.find('li:visible').length != 0) {
			elem.addClass('visible');
		}
	};

	function phoneFormatCheck(value) {
    value = $.trim(value).replace(/\D/g, '');

    if (value.substring(0, 1) == '1') {
        value = value.substring(1);
    }

    if (value.length == 10) {
        return value;
    }
    return false;
	}

	$(document).on('click', '#prototyperst', function() {
		$("#fundformnotice").addClass("hidden");
		$("#fundformnotice").removeClass("visible")
		$("#prototypeRst").removeClass("closed");
		 $(".nigoSubmitBtn").removeAttr('id'); 
	});

	$(document).on('click','#prototypeOnlyrst', function(e) {
		 $("#prototypeRst").removeClass('closed');
		 $("#formReview").addClass('closed'); 
		  
		  $("#alert2").removeClass('visible');

		  $("#prototypebtn").removeClass("closed")
		  
	});
 
	$(document).on('click','#reviewStatusb', function(e) {
		 $("#alert2").addClass('visible');
		 $("#formReview").addClass('closed'); 
		 $("#prototypeRst").addClass('closed');
		 $("#nigobeforeSubmitBtn").addClass('closed'); 
		
	});
	  
	// Incur Fees or Expenses
	$(document).on('click', 'input[name=nextSteps]', function() {
		$value = $(this).attr('value');
		$("#nigobeforeSubmitBtn").removeClass('closed'); 
		 $(".nigoSubmitBtn").removeAttr('id'); 
		if ($value == 'resendForms') {
			$('#resendformsCon').removeClass('closed');
			$('#awaitingFormsCon').addClass('closed');
		}
		else if ($value == 'awaitingForms') {
			$('#resendformsCon').addClass('closed'); 
			$('#awaitingFormsCon').removeClass('closed');
			$("#awaitingCon").html("forms to TIAA?"); 
			$("#textareaCon").html("Awaiting forms notes"); 
			
		}
		else if ($value == 'awaitingDocuments') {
			$('#resendformsCon').addClass('closed');
			$('#awaitingFormsCon').removeClass('closed');
			$("#awaitingCon").html("documents to TIAA?"); 
			$("#textareaCon").html("Awaiting Documents Note"); 
		}
		else if ($value == 'resolveNigo') {
			$('#awaitingFormsCon').addClass('closed');
			$("#textareaCon").html("How has the NIGO issue been resolved?"); 
		}
		else{
		}
		 
	});
	  
    $(document).on('click', '#prototypeOnlyrstnigo_view', function() {
															   
				$(".nigoView2").removeClass('closed');
				$(".nigoView").removeClass('closed');
				$(".nigoView1").addClass('closed');
				$('.igoSubmitBtn4').removeAttr('id');
    });
	
    $(document).on('click', '#addformsClientSub', function() {
															   
                $("#addformsMain").addClass('closed');
				$("#reviewStatusd").removeClass('closed');
				 
				if(formstatusVal=="duplicate_form"){
					$(".no_Further").removeClass('closed'); 
				$(".igo_msg_View").addClass('closed');
				$(".igo_View").addClass('closed');	
				$(".nigoView").addClass('closed');
				}else if(formstatusVal=="formStatus-nigo")
				{
					$(".nigoView").removeClass('closed'); 
				$(".nigoView1").removeClass('closed');
				$(".nigoView2").addClass('closed');
				
			    $(".no_Further").addClass('closed'); 
				$(".igo_msg_View").addClass('closed');
				$(".igo_View").addClass('closed');	
				 
				}else if(formstatusVal=="formStatus-igo")
				{
					    $(".igo_View").removeClass('closed'); 
				$(".nigoView").addClass('closed'); 
			    $(".no_Further").addClass('closed'); 
				$(".igo_msg_View").addClass('closed');
				}else if(formstatusVal=="formStatus-igoMsg")
				{
					    $(".igo_msg_View").removeClass('closed');   
				$(".igo_View").addClass('closed'); 
				$(".nigoView").addClass('closed'); 
			    $(".no_Further").addClass('closed'); 
				}else{
					
				}
			   
    });
		 
	// Incur Fees or Expenses
	$(document).on('click', 'input[name=feesExpenses]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'feesExpenses-yes') {
			$('.feesExpenses_wrapper').removeClass('hidden');
		} else {
			$('.feesExpenses_wrapper').addClass('hidden');
		}
	});

	// Minimum Distribution
	$(document).on('click', 'input[name=minDistReq]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'minDistReq-no') {
			$('.minDistReq_wrapper').removeClass('hidden');
		} else {
			$('.minDistReq_wrapper').addClass('hidden');
		}
	});

	// Process RMD now?
	$(document).on('click', 'input[name=processRMDNow]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'processRMDNow-no') {
			$('.processRMDNow_wrapper').removeClass('hidden');
		} else {
			$('.processRMDNow_wrapper').addClass('hidden');
		}
	});

	// Type of Transfer
	$(document).on('change', 'input[name=typeTransfer]', function() {
		var $id = $(this).attr('id');
		
		if ($id =='typeTransfer-60day') {
			$('.typeDirect').addClass('hidden');
			$('.type60Day').removeClass('hidden');
		} else {
			$('.typeDirect').removeClass('hidden');
			$('.type60Day').addClass('hidden');
		}
	});

	// TPA2Cash or Lump Sum
	$(document).on('click', 'input[name=typeDirectAssetTransfer]', function() {
		var $id = $(this).attr('id'),
				$wrapper = '.' + $id + '_wrapper';
					
		if ($(this).is(':checked')) {
			$($wrapper).removeClass('hidden');
		} else {
			$($wrapper).addClass('hidden');
		}
	});

	//Amount To Transfer
	$(document).on('click', 'input[name=transferEntireAmount]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'transferEntireAmount-yes') {
			$('.transferEntireAmount-Yes_wrapper').removeClass('hidden');
			$('.transferEntireAmount-No_wrapper').addClass('hidden');
		} else {
			$('.transferEntireAmount-No_wrapper').removeClass('hidden');
			$('.transferEntireAmount-Yes_wrapper').addClass('hidden');
		}
	});

	//Fed Withheld
	$(document).on('click', 'input[name=fedWithheld]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'fedWithheld-yes') {
			$('.fedWithheld-Yes_wrapper').removeClass('hidden');
		} else {
			$('.fedWithheld-Yes_wrapper').addClass('hidden');
		}
	});

	//Term Prior Employer
	$(document).on('click', 'input[name=termPriorEmployer]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'termPriorEmployer-yes') {
			$('.termPriorEmployer_wrapper').removeClass('hidden');
		} else {
			$('.termPriorEmployer_wrapper').addClass('hidden');
		}
	});

	//Term Prior Employer
	$(document).on('click', 'input[name=rothAccumulations]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'rothAccumulations-yes') {
			$('.handleRoth_wrapper').removeClass('hidden');
		} else {
			$('.handleRoth_wrapper').addClass('hidden');
		}
	});

	//Term Prior Employer
	$(document).on('click', 'input[name=newAllocations]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'newAllocations-different') {
			$('.selectDifferentAllocations_wrapper').removeClass('hidden');
		} else {
			$('.selectDifferentAllocations_wrapper').addClass('hidden');
		}
	});

	//Fed Withholding Amount
	$(document).on('click', 'input[name=fedWithholdingOption]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'fedWithholdingOption-Yes') {
			$('.howMuchFedWithholdingOption_wrapper').removeClass('hidden');
		} else {
			$('.howMuchFedWithholdingOption_wrapper').addClass('hidden');
		}
	});
	
	//Fed Withholding Amount
	$(document).on('change', '#howMuchFedWithholdingOption', function() {
		$val = $(this).val();
		
		if ($val == 'Percent') {
			$('#fedWithholdingAmount').attr('placeholder', '%').addClass('txtr');
		} else {
			$('#fedWithholdingAmount').attr('placeholder', '$').removeClass('txtr');
		}
	});

	// Date of Leav Heart Act
	$(document).on('click', 'input[name=heartAct]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'heartAct-Yes') {
			$('.dateofLeaveHeartAct_wrapper').removeClass('hidden');
		} else {
			$('.dateofLeaveHeartAct_wrapper').addClass('hidden');
		}
	});

	// Currently Disabled
	$(document).on('click', 'input[name=currentlyDisabled]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'currentlyDisabled-Yes') {
			$('.dateCurrentlyDisabled_wrapper').removeClass('hidden');
		} else {
			$('.dateCurrentlyDisabled_wrapper').addClass('hidden');
		}
	});	

	//Lump Sum Options
	$(document).on('click', 'input[name=rolloverAmountLump]', function() {
		$id = $(this).attr('id');
		$('.rolloverWrapper').addClass('hidden');
		if ($id == 'rolloverAmountLump-Option1') {
			$('.rollover-Option1_wrapper').removeClass('hidden');	
		} else if ($id == 'rolloverAmountLump-Option2') {
			$('.rollover-Option2_wrapper').removeClass('hidden');
		} else if ($id == 'rolloverAmountLump-Option3') {
			$('.rollover-Option3_wrapper').removeClass('hidden');
		}
	});

	// CD Selection on Funding Details
	var $cdSelect = false;
	$(document).on('change', '#investmentTypeDetail', function(){
		var $val = $(this).val();
		if ($val == 'cd') {
			$cdSelect = true;
		} else {
			$cdSelect = false;
		}
		
		if ($cdSelect) {
			$('.nonCDContent').addClass('hidden');
			$('.CDContent').removeClass('hidden');
		}
	});
	
	$(document).on('change', '#transferType', function() {
		// Reset Authorization Requirements Form
		$('.tiaaSendForms_wrapper, .thirdPartyForms_wrapper, .thirdParty_wrapper, .carrierForms_wrapper, .timeUntilClientReceives_wrapper, .selectSignatory_wrapper, .medallionSignature_wrapper, .howFormsAccepted_wrapper, .sendMethod_wrapper, .returnMethod_wrapper, .howFundsSent_wrapper, .followUp_wrapper, .turnAround_wrapper, .sourceContactInfo_wrapper').addClass('hidden');
		$('.authRequirementsForm').find("input[type=text], select").val("");
		$('.authRequirementsForm input[type="radio":checked]:not("input[name=custAuth]")').each(function(){
      $(this).prop('checked', false);
  	});
  	$('.authRequirementsForm input[name=custAuth]').prop('checked', false);
  	// Reset Forms Handling Summary Panel
  	formsHandlingSummaryUpdate('clientAuthReqIndicator', 'TBD');
		formsHandlingSummaryUpdate('reqFormsIndicator', 'TBD');
		formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'TBD');
		formsHandlingSummaryUpdate('delMethodCarierIndicator', 'TBD');
		formsHandlingSummaryUpdate('delMethodClientIndicator', 'TBD');

		var $val = $(this).val();
		authReqModel['transferTypeVal'] = $val;
		console.log('Transfer type: ' + authReqModel['transferTypeVal']);
		if ($val.indexOf('directTransfer')) {
			$('.accountTypeDirect').addClass('hidden');
			$('.accountTypeOther').removeClass('hidden');
		} else {
			$('.accountTypeDirect').removeClass('hidden');
			$('.accountTypeOther').addClass('hidden');
		}
		if (!$val.indexOf('directRollover')) {
			$('.rothTypeDirect').addClass('hidden');
			$('.rothTypeOther').removeClass('hidden');
		} else {
			$('.rothTypeDirect').removeClass('hidden');
			$('.rothTypeOther').addClass('hidden');
		}
	});

	$(document).on('change', '#transferTypeCIH', function() {
		var $val = $(this).val();
		$('.accountTypeIRC_wrapper').removeClass('hidden');
		// Hide all
		$('.accountTypeDirect').addClass('hidden');
		$('.accountTypeOther').addClass('hidden');
		$('.accountType60').addClass('hidden');
		
		if ($val === 'directTransferCash') {
			$('.accountTypeDirect').removeClass('hidden');
		} else if ($val === 'directRolloverCash') {
			$('.accountTypeOther').removeClass('hidden');
		} else if ($val === '60DayRollover') {
			$('.accountType60').removeClass('hidden');
		}
		if (!$val.indexOf('60DayRollover')) {
			$('.fundsAllocation_wrapper').addClass('hidden');
			$('.firmMailingAddress_wrapper').addClass('hidden');
		} else {
			$('.fundsAllocation_wrapper').removeClass('hidden');
			$('.firmMailingAddress_wrapper').removeClass('hidden');
		}
	});
 
	$(document).on('click', '#searchBtn', function() { 
		$('.srhResultsDiv').removeClass('hidden');
	});
	
	
	/*Unsolicited forms*/
	 
$('.selectNigo').on('click', function() {
    var $row = jQuery(this).closest('tr');
    var $columns = $row.find('td');

    $columns.addClass('row-highlight');
    var values = "";
    
    jQuery.each($columns, function(i, item) {
        values = $columns[i].innerHTML;
		 
	        $("#addressLine1").val($columns[1].innerHTML);
		    $("#addressLine2").val($columns[2].innerHTML);
		    $("#city").val($columns[3].innerHTML); 
			$('#state_Input').val("IL"); 
		   // $("#state_Input").val($columns[4].innerHTML);
		    $("#zipCode").val($columns[5].innerHTML);
		    //$("#phone").val($columns[6].innerHTML); 
			 
    });
    $("#unsol_formsnigo_addresslookup").dialog('close');
});

$('.selectIgo').on('click', function() {
    var $row = jQuery(this).closest('tr');
    var $columns = $row.find('td');

    $columns.addClass('row-highlight');
    var values = ""; 
    jQuery.each($columns, function(i, item) {
        values = $columns[i].innerHTML;
		 
	        $("#addressLine11").val($columns[1].innerHTML);
		    $("#addressLine22").val($columns[2].innerHTML);
		    $("#city2").val($columns[3].innerHTML); 
			$('#state_Input2').val("IL"); 
		   // $("#state_Input").val($columns[4].innerHTML);
		    $("#zipCode2").val($columns[5].innerHTML);
		    //$("#phone").val($columns[6].innerHTML); 
			 
    });
    $("#unsol_formsigo_addresslookup").dialog('close');
});

$('#unsoli_saveSuccess1').on('click',function(e){
						 
		var flag=0; 
		$("#unsolnigoerrmessage").html('');
		if($("#unsolnigocontract_Number").val() == 'Select'){
			$("#unsolnigoerrmessage").append("<li>Contract Number field empty</li>");
			$("#unsolnigocontract_Number").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
		}
		else{
			$("#unsolnigocontract_Number").closest("div.lblFieldPairV").removeClass("alertHighlight");
		} 
		   if($("#accountType").val() == ''){
			//$("#unsolnigoerrmessage").append("<li>Account Type (IRC) field empty</li>");
			//$("#accountType").closest("div.lblFieldPairV").addClass("alertHighlight");
			//flag =1;
		}
		else{
			//$("#accountType").closest("div.lblFieldPairV").removeClass("alertHighlight");
		} 
		
		    if($("#transferType").val() == ''){
			//$("#unsolnigoerrmessage").append("<li>Transfer Type (IRC) field empty</li>");
			//$("#transferType").closest("div.lblFieldPairV").addClass("alertHighlight");
			//flag =1;
		}
		else{
			//$("#transferType").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
		 
		if(flag == 1){
			$("#unsolnigovalidationErrmsg").removeClass("hidden");
			$("#unsolnigovalidationErrmsg").addClass("visible");
			
			$("#unnigosuccess1").removeClass("visible");
			$("#unnigosuccess1").addClass("hidden");
		}
		else{
			$("#unsolnigovalidationErrmsg").addClass("hidden");
			$("#unsolnigovalidationErrmsg").removeClass("visible");
			
			$("#unnigosuccess1").removeClass("hidden");
			$("#unnigosuccess1").addClass("visible");
			 
		} 
    });

$('#unsoli_saveSuccess2').on('click',function(e){
						 
		var flag=0; 
		$("#nongolderrmessage2").html('');
		if($("#addressLine11").val() == ''){
			$("#nongolderrmessage2").append("<li>Address Line1 field empty</li>");
			$("#addressLine11").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
		}
		else{
			$("#addressLine11").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
		
			if($("#unsoligocontract_Number").val() == 'Select'){
			$("#nongolderrmessage2").append("<li>Contract Number field empty</li>");
			$("#unsoligocontract_Number").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
		}
		else{
			$("#unsoligocontract_Number").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
		
		if(jQuery('input[name=sendFunds_transfer]').is(':checked')){
        $('input[name=sendFunds_transfer]').closest("div.lblFieldPairV").removeClass("alertHighlight");
        }else{
       	$("#nongolderrmessage2").append("<li>Please select Send funds to TIAA via wire transfer</li>");
			$('input[name=sendFunds_transfer]').closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
        }
 
 
	    if($("#accountType").val() == ''){
			//$("#nongolderrmessage2").append("<li>Account Type (IRC) field empty</li>");
			//$("#accountType").closest("div.lblFieldPairV").addClass("alertHighlight");
			//flag =1;
		}
		else{
			//$("#accountType").closest("div.lblFieldPairV").removeClass("alertHighlight");
		} 
		
		    if($("#transferType").val() == ''){
			//$("#nongolderrmessage2").append("<li>Transfer Type (IRC) field empty</li>");
			//$("#transferType").closest("div.lblFieldPairV").addClass("alertHighlight");
			//flag =1;
		}
		else{
			//$("#transferType").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
		
		
		
		    if($("#planNameunsol2").val() == ''){
			$("#nongolderrmessage2").append("<li>Plan Number field empty</li>");
			$("#planNameunsol2").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
		}
		else{
			$("#planNameunsol2").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
 
	    if($("#account_Number2").val() == ''){
			$("#nongolderrmessage2").append("<li>Account Number field empty</li>");
			$("#account_Number2").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
		}
		else{
			$("#account_Number2").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
         if($("#carrierName2").val() == ''){
			$("#nongolderrmessage2").append("<li>Alternate Carrier Name field empty</li>");
			$("#carrierName2").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
		}
		else{
			$("#carrierName2").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
	    if($("#amount2").val() == ''){
			$("#nongolderrmessage2").append("<li>Amount field empty</li>");
			$("#amount2").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
		}
		else{
			$("#amount2").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
		 
		if($("#city2").val() == ''){
			$("#nongolderrmessage2").append("<li>City field empty</li>");
			$("#city2").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag=1;
		}	
		else{
			$("#city2").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}

		if($("#state_Input2").val() == ''){
			$("#nongolderrmessage2").append("<li>State field empty</li>");
			$("#state_Input2").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag=1;
		}
		else{
			$("#state_Input2").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}		

		if($("#zipCode2").val() == ''){
			$("#nongolderrmessage2").append("<li>zipcode  field empty</li>");
			$("#zipCode2").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag=1;
		}	
		else{
			$("#zipCode2").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}
 
		if(flag == 1){
			$("#validationErrormsg2").removeClass("hidden");
			$("#validationErrormsg2").addClass("visible");
		}
		else{
			$("#validationErrormsg2").addClass("hidden");
			$("#validationErrormsg2").removeClass("visible");
			
			$("#unnigosuccess2").addClass("visible");
			$("#unnigosuccess2").removeClass("hidden");
			 
		}
		  
		if( (addressLine1=="") || (city=="") || (state_Input=="") || (zipCode=="") || (phone=="") ){
		$("#validationErrormsg").addClass('visible'); 
		 
		}else{  
	   
		$("#validationErrormsg").removeClass('visible');
		} 
    }); 
		$(document).on('change',"#unsolnigocontract_Number",function (e) { 
																		
         var optVal= $("#unsolnigocontract_Number option:selected").val();
		 $('#planNameunsol1').val("E77999011");
        })
	
	    $(document).on('change',"#unsoligocontract_Number",function (e) { 
         var optVal= $("#unsoligocontract_Number option:selected").val();
		 $('#planNameunsol2').val("E77999011");
        })
	 
		$(document).on('click', '#unsolnigo_searchBtn', function() { 
		$('.unsolsrhResultsDivnigo').removeClass('hidden');
	});
		
			$(document).on('click', '#unsoligo_searchBtn', function() { 
		$('.unsolsrhResultsDivigo').removeClass('hidden');
	});
	 
    $(document).on('click', '#cdyes_input', function() { 
		$('.liquidatecashout').removeClass('hidden');
	});
	$(document).on('click', '#cdno_input', function() {
      
		$('.liquidatecashout').addClass('hidden');
		$('.maturitydateCd').addClass('hidden'); 
	});
	 
	$(document).on('click', '#immediately', function() { 
		$('.maturitydateCd').addClass('hidden');
	});
	
    $(document).on('click', '#waitformaturity', function() { 
		$('.maturitydateCd').removeClass('hidden');
	});
	 
	//unsolicited Deposit Cd nigo//
	
    $(document).on('click', '#cdyes_input1', function() { 
		$('.liquidatecashout1').removeClass('hidden');
	});
	$(document).on('click', '#cdno_input1', function() {
      
		$('.liquidatecashout1').addClass('hidden');
		$('.maturitydateCd1').addClass('hidden'); 
	});
	 
	$(document).on('click', '#immediately1', function() { 
		$('.maturitydateCd1').addClass('hidden');
	});
	
    $(document).on('click', '#waitformaturity1', function() { 
		$('.maturitydateCd1').removeClass('hidden');
	});
	
	//unsolicited Deposit Cd igo//
    $(document).on('click', '#cdyes_input2', function() { 
		$('.liquidatecashout2').removeClass('hidden');
	});
	$(document).on('click', '#cdno_input2', function() {
      
		$('.liquidatecashout2').addClass('hidden');
		$('.maturitydateCd2').addClass('hidden'); 
	});
	 
	$(document).on('click', '#immediately2', function() { 
		$('.maturitydateCd2').addClass('hidden');
	});
	
    $(document).on('click', '#waitformaturity2', function() { 
		$('.maturitydateCd2').removeClass('hidden');
	});
	
	 
	// IGO/NIGO - Authorization Requirements
	$(document).on('change', 'input[name=custAuthRO]', function() {
		$('.authRequirementsROForm').addClass('hidden');
		$('.authRequirementsForm').removeClass('hidden');
	});
	// IGO/NIGO - Funding Forms Review
 
	$(document).on('change', 'input[name=overnight]', function() {
		var $id = $(this).attr('id');
		if ($id === 'overnight-yes') {
			$("#tranfer-yes").attr("Disabled",true);
			$("#tranfer-yes").attr("checked",false);

		}
		else{
			$("#tranfer-yes").attr("Disabled",false)
		}
	});

	$(document).on('change', 'input[name=altCarrier]', function() {
		var $id = $(this).attr('id');
		if ($id === 'altCarrier-yes') {
			$("#altCarriercopy-yes").attr("Disabled",true);
			$("#altCarriercopy-yes").attr("checked",false);
		}
		else{
			$("#altCarriercopy-yes").attr("Disabled",false)
		}
	});
 
	$(document).on('click', '#tranfer-yes', function() {
		$("#faxtext").removeClass("hidden");
	});
	$(document).on('click', '#tranfer-no', function() {
		$("#faxtext").addClass("hidden");
	});
 
	$(document).on('click', '#cancelrequest', function() { 
		window.location.replace("fundcancel.html")
	});

	$(document).on('click', '#closePopup', function() {
		$('#closePopup').dialog('close');
	});

	$(document).on('change', 'input[name=fundform]', function() {
		var $id = $(this).attr('id');
		if ($id === 'fundform-nigo') {
			$('.nigo_fund').removeClass('closed');
			$('.igo_fund').addClass('closed');
		} 
		else if ($id === 'fundform-igo') {
			$('.igo_fund').removeClass('closed');
			$('.nigo_fund').addClass('closed');
			$(".formfundinstruction").html("Provide instructions for the Imaging Transfers Team to complete preparations for the alternate carrier package.");
		} 	
		else if ($id === 'fundform-igoMsg') {
			$('.igo_fund').removeClass('closed');
			$('.nigo_fund').addClass('closed');
			$(".formfundinstruction").html("Provide instructions for the Imaging Transfers Team to complete MSG Delivery and alternate carrier package period");
		} 	
		else if ($id === 'fundform-noaction') {
			
			$('.nigo_fund').addClass('closed');
			$('.nigo_noaction').removeClass('closed');
		}		
		$("#fundformbar").removeClass("closed");
		$("#nigobeforeSubmitBtn").removeClass("closed");

		
	});
	 
	$(document).on('change', 'input[name=formStatus]', function() {
		var $id = $(this).attr('id');
		
		formstatusVal=$id;
		
		
		if ($id === 'formStatus-noaction') {
			$(".nofurtheraction_wrapper").addClass("visible");  
			$(".nofurtheraction_wrapper").removeClass('hidden');
			
			$('.igo_wrapper').addClass('hidden');
			$("#formreviewalert").addClass("hidden");
			$(".nofurtheraction_wrapper").addClass("hidden"); 			
			$('.nigo_wrapper').addClass('hidden'); 
			$("#batchnumber").addClass("closed");
			$("#formreviewalertnigo").addClass("hidden");
			$("#nigoAlert").removeClass("visible");
			$("#nigoAlert2").removeClass("visible");
			$("#nigoAlert2").addClass("hidden");
			$("#nigoAlert3").addClass("hidden"); 
			$("#nigoAlert3").removeClass("visible");
			$(".nofurtheraction_wrapper").addClass("hidden"); 
			
			$("#addFormBtn2").addClass("closed");
			$("#fundingformbar").addClass("closed");
	
			
		}
		else if ($id === 'formStatus-nigo') {
			$('.igo_wrapper').addClass('hidden');
			$('.nigo_wrapper').removeClass('hidden');
	
			$("#batchnumber").removeClass("closed");
			
			$("#formreviewalert").removeClass("visible");
			$("#formreviewalert").addClass("hidden");
			$("#formreviewalertnigo").removeClass("hidden");
			$("#formreviewalertnigo").addClass("visible");  
			
			$("#fundingformbar").addClass("closed"); 
			$("#addFormBtn2").removeClass("closed"); 
		    $("#nigoAlert").addClass("visible"); 
		    $('#alertCont').html("NIGO");
			
			$("#nigoAlert2").removeClass("hidden"); 
			$("#nigoAlert2").addClass("visible");
			$("#nigoAlert3").removeClass("hidden"); 
			$("#nigoAlert3").addClass("visible");
			$("#duplicate_forms").removeClass("visible");
			
			$(".nofurtheraction_wrapper").addClass("hidden"); 
			$(".nofurtheraction_wrapper").removeClass("visible");
		      
		} 
		else if ($id === 'duplicate_form') {
			$('.igo_wrapper').addClass('hidden');
			$('.nigo_wrapper').addClass('hidden'); 
			$("#batchnumber").addClass("closed");
		  
			$("#duplicate_forms").addClass("visible");
			
			$("#formreviewalert").removeClass("hidden");
			$("#formreviewalert").addClass("visible");
			$("#formreviewalertnigo").removeClass("visible");
			$("#formreviewalertnigo").addClass("hidden"); 
			  
			$("#fundingformbar").removeClass("closed");	
			$("#addFormBtn2").removeClass("closed");
			$("#nigoAlert").removeClass("visible");
			
		   $("#nigoAlert2").removeClass("visible"); 
			 $("#nigoAlert2").addClass("hidden");
			 $("#nigoAlert3").addClass("hidden"); 
			 $("#nigoAlert3").removeClass("visible");
			 $(".nofurtheraction_wrapper").addClass("hidden"); 
			 $(".nofurtheraction_wrapper").removeClass("visible");
		}

		else {
			$('.nigo_wrapper').addClass('hidden');
			$('.igo_wrapper').removeClass('hidden');
			$("#formreviewalert").addClass("visible");
			$("#batchnumber").removeClass("closed");
			$("#formreviewalert").removeClass("hidden");
			$("#fundingformbar").removeClass("closed");	
			$("#addFormBtn2").removeClass("closed");
			$("#duplicate_forms").removeClass("visible");
			$(".nofurtheraction_wrapper").addClass("hidden"); 
			$(".nofurtheraction_wrapper").removeClass("visible");			
		}

		if ($id === 'formStatus-igoMsg') {
			$('.imagingTeamInstructions').text('Provide instructions for the Imaging Transfers Team to complete MSG Delivery and alternate carrier package period.');
			$('#alertCont').html("NIGO");
			
			
			$("#nigoAlert").addClass("visible");
				$("#formreviewalert").removeClass("visible");
			$("#formreviewalert").addClass("hidden");
			$("#formreviewalertnigo").removeClass("visible");
			$("#formreviewalertnigo").addClass("hidden"); 
			
			 $("#nigoAlert2").removeClass("visible"); 
			 $("#nigoAlert2").addClass("hidden");
			 $("#duplicate_forms").removeClass("visible");
			  $("#igoradioBtn input:radio").attr('disabled',false);
			  $("#igoradioBtn2 input:radio").attr('disabled',false);
		} 

		else if ($id === 'formStatus-igo')  {
			$('.imagingTeamInstructions').text('Provide instructions for the Imaging Transfers Team to complete alternate carrier package period. ');
			$('#alertCont').html("IGO");$("#nigoAlert").addClass("visible");
			
			$("#formreviewalert").removeClass("visible");
			$("#formreviewalert").addClass("hidden");
			$("#formreviewalertnigo").removeClass("visible");
			$("#formreviewalertnigo").addClass("hidden"); 
			
			 $("#nigoAlert2").removeClass("visible"); 
			 $("#nigoAlert2").addClass("hidden");
			
			$("#duplicate_forms").removeClass("visible");
			
			$("#igoradioBtn input:radio").attr('disabled',true);
			$("#igoradioBtn input:radio").attr('checked', false);
			
			$("#igoradioBtn2 input:radio").attr('disabled',true);
			$("#igoradioBtn2 input:radio").attr('checked', false);
			  
		}
		else{
			$('.imagingTeamInstructions').text('');
		}
	});
	 
	$(document).on('change', 'input[name=unsolicitedformStatus]', function() {
		var $id = $(this).attr('id');
	  
		if ($id === 'unsolicitedform-nigo') {
			
			$("#unsolicitedNigo").removeClass("hidden");
			$("#unsolicitedNigo").addClass("visible"); 
			$("#unsolicitedigo").removeClass("visible"); 
			$("#unsolicitedigo").addClass("hidden");
	 
		} 

		else if ($id === 'unsolicitedform-igo')  {
		  
			$("#unsolicitedNigo").removeClass("visible");
			$("#unsolicitedNigo").addClass("hidden"); 
			$("#unsolicitedigo").removeClass("hidden"); 
			$("#unsolicitedigo").addClass("visible");
		   
		}
		else{
		 
		}
	});
	 
	// MSG IGO/NIGO - Funding Forms Review
	$(document).on('change', 'input[name=nigoFormStatus]', function() {
		var $id = $(this).attr('id');
		if ($id === 'nigoFormStatus-nigo') {
			$('.msgNigo_wrapper').removeClass('hidden');
			$('.msgIgo_wrapper').addClass('hidden');
		} else {
			$('.msgNigo_wrapper').addClass('hidden');
			$('.msgIgo_wrapper').removeClass('hidden');
		}
	});
	//MSG NIGO Error Check
	$(document).on('click', '.msgNigoSubmitBtn', function() {
		if ($('#msgNigoComment').val() == '') {
	    $('.msgNigoError').removeClass('hidden');
	    $('#msgNigoComment').prev().addClass('alertHighlight');
		} else {
	   	$('.msgNigoError').addClass('hidden');
	   	$('#msgNigoComment').prev().removeClass('alertHighlight');
	   	$('.msgIgo_wrapper').addClass('hidden');
	   	$('.nigoConfirmation').addClass('visible');

		}
	});
	// IGO/NIGO Show Other 
	$(document).on('click', 'input[name=otherNIGOReasons]', function() {
		if ($(this).is(':checked')) {
			$('.otherNIGOComment_wrapper').removeClass('hidden');
		} else {
			$('.otherNIGOComment_wrapper').addClass('hidden');
		}
	});

	$(document).on('click', '.nofurtherSubmitBtn', function() {
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').addClass('hidden');

		$('.nigoHeader').addClass('hidden');			
		$('.nofurtheractionSubmitted').removeClass('hidden');				
	});
	// Show/Hide IGO/NIGO Submit
	$(document).on('click', '.nigoSubmitBtn', function() {
		//NIGO Error Check
		if ($('.nigo_wrapper input:checkbox:checked').length > 0) {
	    $('.nigoError').removeClass('visible');
	    $('.nigoSubmitted').removeClass('hidden');
			$('.nigo_wrapper').addClass('hidden');
			$('.nigoTSS').addClass('hidden');
			$('.igoSubmitted').addClass('hidden');
			$('.igo_wrapper').addClass('hidden');
			$('.nigoHeader').addClass('hidden');
		} else {
	   	$('.nigoError').addClass('visible');
		}
	});
	$(document).on('click', '.nigoEditBtn', function() {
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').removeClass('hidden');
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').addClass('hidden');
		$('.nigoHeader').removeClass('hidden');
	});
	$(document).on('click', '.igoSubmitBtn', function() {
		$('.igoSubmitted').removeClass('hidden');
		$('.igo_wrapper').addClass('hidden');
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.nigoHeader').addClass('hidden');
	});
	$(document).on('click', '.igoEditBtn', function() {
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').removeClass('hidden');
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.nigoHeader').removeClass('hidden');
	});
	$(document).on('click', '.hiddenRST', function() {
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').addClass('hidden');
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.nigoHeader').addClass('hidden');
		$('.nigoRST').removeClass('hidden');
	});
	$(document).on('click', '.hiddenTSS', function() {
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').addClass('hidden');
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.nigoHeader').removeClass('hidden');
		$('.nigoTSSConfirm').addClass('hidden');
		$('.nigoTSS').removeClass('hidden');
		$('input[name=formStatus]').removeAttr('checked');
	});
	// IGO/NIGO - NIGO RST
	$(document).on('change', 'input[name=nextSteps]', function() {
		var $id = $(this).attr('id');
		$('.rst_wrapper').addClass('hidden');
		if ($id === 'nextSteps-resend') {
			$('.resend_wrapper').removeClass('hidden');
		} else if ($id === 'nextSteps-awaitingForms') {
			$('.awaitingForms_wrapper').removeClass('hidden');
		} else if ($id === 'nextSteps-awaitingDocs') {
			$('.awaitingDocs_wrapper').removeClass('hidden');
		} else if ($id === 'nextSteps-resolve') {
			$('.resolve_wrapper').removeClass('hidden');
		}
	});
	
	// IGO/NIGO - RST Confirm
	$(document).on('click', '.submitRSTBtn', function(){
		$('.nigoRST').addClass('hidden');
		$('.nigoRSTConfirm').removeClass('hidden');
	});

	// IGO/NIGO - TSS Confirm
	$(document).on('click', '.submitTSSBtn', function(){
		$('.nigoRST').addClass('hidden');
		$('.nigoTSSConfirm').removeClass('hidden');
	});


	// LOA Show Letter Link
	$(document).on('change', '#letterOfAcceptance', function() {
		$('.loaDoc_wrapper').removeClass('hidden');
	});

	// Save & Exit Check
	$(document).on('click', '#saveExitBtn', function() {
		if (!$('input[name=loaPrinted]').is(':checked')) {
			$('.loaAlertText').removeClass('hidden');
		} else {
			$('.loaAlertText').addClass('hidden');
		}
	});

	// Other Authorizations
	$(document).on('change', 'input[name=otherAuth]', function() {
		var $id = $(this).attr('id');

		if ($id === 'otherAuth-yes') {
			$('#otherInput').removeClass('hidden');
		} else {
			$('#otherInput').addClass('hidden');
		}
	});
 
	// Check that Maturity Date selected is not beyond 180 days
	$(document).on('change', '#maturityDate', function() {
			var $selectedDate = $(this).datepicker('getDate'),
					fullDate = new Date(),
					diffDate = ($selectedDate - fullDate)/1000/60/60/24;

			if (diffDate > 180) {
				$("#popupMaturity").dialog('open');
			}			
	});
	
	// Populate Bank Accoutn on File - SIM Code, NOT for DEV
	$(document).on('change', '#acctOnFile', function(){
		$('#acctType-checking').prop("checked", true);
		$('#acctHolderFName').val('Penelope');
		$('#acctHolderMName').val('');
		$('#acctHolderLName').val('Pension');
		$('#bankRoutingNumber').val('000000186');
		$('#bankAccountNumber').val('000000529');
	});
 
	// Auto opening/closing panels for Funding Edit Details
	$(document).on('click', '.transferDetailsPanelBtn', function() {
		$(document).find('a#transferDetailsPanel').trigger('click');
		$(document).find('a#authRequirementsPanel').trigger('click');
	});
	$(document).on('click', '.authRequirementsPanelBtn', function() {
		$(document).find('a#authRequirementsPanel').trigger('click');
		$(document).find('a#handlingInstructionsPanel').trigger('click');
	});
 
	$(document).on('click', '#continueDetailBtn', function(e) {
		e.stopPropagation();
		errorWrapper = ('#' + $('#tDetailsAlerts').attr('id'));
		$(".transferDetailWrapper").find('[data-required=true]').each(function() {
			var element = $(this),
					elementVal = $.trim(element.val());
			
			if (element.is(':hidden')) { 
				requiredFieldSuccess(element, errorWrapper);
			} else {
				if (element.is(':checkbox') || element.is(':radio')) { //If Radio or Checkbox
					if (!$(element).is(":checked")) {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}
				} else { // All other inputs
					if (elementVal == 0 || typeof elementVal == "undefined" || elementVal == "") {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}	
				}
			}
		});
		return false;
	});
 
	$(document).on('click', '#release_updateDetailBtn', function(){
		$("#inputDetails").removeClass("hidden");
		$("#inputReadOnly").addClass("hidden");
		$("#inputReadOnly").removeClass("visible");
	});
 
	$(document).on('click', '#release_confirmDetailBtn', function(){
		$(".companyNameDetail").html($("#companyNameDetail").val())
		$(".productNameDetail").html($("#productNameDetail").val())
		$(".investmentTypeDetail").html($("#investmentTypeDetail").val())
		$(".transferValueDetail").html($("#transferValueDetail").val())
		$(".acctContractDetail").html($("#acctContractDetail").val())

		$("#inputDetails").addClass("hidden");
		$("#inputDetails").removeClass("visible"); 
		$("#inputReadOnly").removeClass("hidden");	
		$("#inputReadOnly").addClass("visible"); 
		$(".transferDetailWrapper").removeClass("closed")

	});
 
	$(document).on('click', '#confirmDetailBtn', function(e) {
		e.stopPropagation();
		errorWrapper = ('#' + $('#sourceDetailErrors').attr('id'));
		$("#inputDetails").find('[data-required=true]').each(function() {
			var element = $(this),
					elementVal = $.trim(element.val());
 
			if (element.is(':hidden')) { 
				requiredFieldSuccess(element, errorWrapper);
			} else {
				if (element.is(':checkbox') || element.is(':radio')) { //If Radio or Checkbox
					if (!$(element).is(":checked")) {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}
				} else { // All other inputs
					if (elementVal == 0 || typeof elementVal == "undefined" || elementVal == "") {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}	
				}
			}
		});
		return false;
	});
	// END - Forms Validation for Transfer Details 
    //Paper Signature forms Radio Btn//  
    $(document).on('click','input[name=customerAuth]',function(){
		if($(this).is(':checked')) {  
		 closeContent();
		  var dataReq=$(this).attr('data-req'); 
		  if((dataReq=="paper_Signature") || (dataReq=="digitallyForm")){
			  $(".carrierForms_wrapper").removeClass('closed');
			  $(".howFundsSent_wrapper").addClass('hidden');
		  } 
		  if(dataReq=="phoneAuthorization"){
			  $(".howFundsSent_wrapper").removeClass('hidden');
			  $(".carrierForms_wrapper").addClass('closed');
		  } 

		} 
	});
	
	$(document).on('click','input[name=wtradio]',function(){
		if($('#wtradiono').is(':checked')) { 
		 	$(".wtradionoDiv").removeClass('hidden');
		}
		if($('#wtradioyes').is(':checked')) { 
		 	$(".wtradionoDiv").addClass('hidden');
		}
	});
	$(document).on('click','input[name=allocationChoice]',function(){
		if($('#allocationChoiceNo').is(':checked')) { 
		 	$(".contributionsMadeSoFar").removeClass('hidden');
		} else {
		 	$(".contributionsMadeSoFar").addClass('hidden');
		}
	});
	 
	// Show/hide for Create New funding Request
	$(".createNewFundingRequest").click(function() {
	    $(".createNewFundingRequest").addClass("hidden");
	    $(".accountToFund-wrapper").removeClass("hidden");
	}); 

	// Show/Hide Funding Sources for Fund an Account
	var $origVal = '';
	var $newVal = '';
	$('.addFundingToConfirmBtn').on('click', function() {
		var $val = $("#addFundingTo").val();
		if ($val != "") {
			$('.panels').removeClass("hidden");
			$("#alertText2").addClass("hidden");
			$("#addFundingTo").parent().removeClass("alertHighlight");
			$(".addFundingToConfirmBtn").addClass("hidden");
			$("#addFundingTo option[value='']").remove();
			$origVal = $("#addFundingTo").val();
			//$("#addFundingTo").attr("id", "addFundingToChange");
			$('.orchestrationId_wrapper').removeClass('hidden');
		} else {
			$('.panels').addClass("hidden");
			$("#alertText2").removeClass("hidden");
			$("#addFundingTo").parent().addClass("alertHighlight");
		}
	});
	 
	$('.addFundingToConfirmBtn1').on('click', function() {
		var $val = $("#addFundingTo1").val();
		  
		if ($val != "") {  
			$("#alertText1").addClass("hidden");
			$("#addFundingTo1").parent().removeClass("alertHighlight");
			$(".addFundingToConfirmBtn1").addClass("hidden"); 
			$("#addFundingTo1 option[value='']").remove(); 
			//$("#addFundingTo1").attr("id", "addFundingToChange1"); 
			$('.orchestrationId_wrapper1').removeClass('hidden');
			$('.orchestrationId_wrapper').removeClass('hidden'); 
			$('#external_Rollover').addClass("hidden");
			
		} else {
		    $("#addFundingTo1").parent().addClass("alertHighlight"); 
			$("#alertText1").removeClass("hidden");
			$('#external_Rollover').addClass("hidden");
		}
	});
	 
	$(document).on('change', '#addFundingToChange', function(){
		$newVal = $("#addFundingToChange").val();
		if ($newVal != $origVal) {
			$('#eraseRequest').dialog('open');
		}
	});
	$('#yesChangeBtn').on('click', function() {
		$origVal = $("#addFundingToChange").val();
		$('#eraseRequest').dialog('close');
	});
	$('#noChangeBtn').on('click', function() {
		$("#addFundingToChange").val($origVal);
		$('#eraseRequest').dialog('close');
	});

	// Show Bank Info
	$(document).on('change', 'input[name="eftProvide"]', function() {
	  var $id = $(this).attr('id');
		if($id == 'eftProvide-phone') {
			$('.bankInfo_wrapper').removeClass('hidden');
			//$('.recordedLineAlert').addClass('visible');
	  } else {
	  	$('.bankInfo_wrapper').addClass('hidden');
	  	//$('.recordedLineAlert').removeClass('visible');
	  }
	});

	// Show hide External rollovers
	$("input[name='external-rollovers']").click(function() {
	  var $id = $(this).attr("id");
		if($id == "external-rollovers-yes") {
			$('.extSources-wrapper').removeClass('hidden');
	  } else {
	  	$('.extSources-wrapper').addClass('hidden');
	  }
	});

	// Show hide External rollovers
	$("input[name='internal-rollovers']").click(function() {
	  var $id = $(this).attr("id");
		if($id == "internal-rollovers-yes") {
			$('.intSources-wrapper').removeClass('hidden');
	  } else {
	  	$('.intSources-wrapper').addClass('hidden');
	  }
	});

	// Search Type, on change populate Business Units Dropdown
  $('#companyName').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('.otherCompany-wrapper').removeClass('hidden');
    } else {
    	$('.otherCompany-wrapper').addClass('hidden');
    }
  });

  // Search Type, on change populate Business Units Dropdown
  $('#companyNameDetail').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('#otherCompany').removeClass('hidden');
    	$('#companyNameDetail').removeClass('inputxlg').addClass('inputs');
    } else {
    	$('#otherCompany').addClass('hidden');
    	$('#companyNameDetail').addClass('inputxlg').removeClass('inputs');
    }
  });

  // Search Type, on change populate Business Units Dropdown
  $('#productNameDetail').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('#otherProduct').removeClass('hidden');
    	$('#productNameDetail').removeClass('inputxlg').addClass('inputs');
    } else {
    	$('#otherProduct').addClass('hidden');
    	$('#productNameDetail').addClass('inputxlg').removeClass('inputs');
    }
  });

	
	// Add External Sources to Table
	$(document).on('click', '#addBtn', function(e) {
		var $companyName = $('#companyName').val(),
				$otherCompany = $('#otherCompany').val(),
				$company = ($companyName == "Other") ? $otherCompany : $companyName,
				$checkInHand = $('#clientCheck').is(':checked') ? true : false,
				$transferAmount = $('#transferAmount').val(),
				row = '<tr>' +
								'<td>' + $company + '</td>' +
                '<td>' + $transferAmount + '</td>' +
                '<td></td>' +
                '<td></td>' +
                '<td>92Z1776H21</td>' +
                '<td class="status"><span class="down">Incomplete</span></td>' +
                '<td>' +
                  '<a href="edit_funding_detail.html" class="editBtn">Edit</a>' +
                  '<span class="dim pls prs">|</span>' +
                  '<a href="#cancelExtSourcePopup" class="cancelBtn">Remove</a>' +
                '</td>' +
              '</tr>';
		// Initialize Table & Form
    $('.noDataToDisplay').remove();
    // Add collected data
    $('#tbl_extsources0 tbody').append(row);
    // Close dialog
    $('#addExternalsourcePopup').dialog('close');
    // Clear Form for next use
    $(this).closest('form').find("input[type=text], select").val("");

    // If Check In Hand is selected, go to Edit Detail page
    if ($checkInHand) {
    	location  = "edit_funding_detail_cih.html?release=sprint17&toolbar=true";
    }
  });

  

// // Begin Add/Remove Rows for Beneficiaries

//   // Remove External Sources from table
//   var $rowToRemove = '';
//   $(document).on('click', '.cancelBtn', function(e){
//   	//Show Dialog
//   	$('#cancelExtSourcePopup').dialog('open');
//   	// Set var to which row being removed
//   	$rowToRemove = $(this).closest('tr');
//   });


// 	$(document).on('click', '#cancelSourceBtn', function(e) {
// 		// Remove row designated by var
// 		$rowToRemove.remove();
// 		// Default placeholder row
// 		var row = '<tr>' +
//               	'<td colspan="7" class="noDataToDisplay">No data to display</td>' +
//               '</tr>';
//     // If table is empty, replace with placeholder row
//     if ($('#tbl_extsources0 tbody tr').children().length <= 1) {
//     	$('#tbl_extsources0 tbody').append(row);
//     }
//     //Close dialog 
//     $('#cancelExtSourcePopup').dialog('close');
//     // Re-Init var
//     $rowToRemove = '';
//   });
// // Ended Add/Remove Rows for Beneficiaries
   
  // Set External Sources to Canceled
  var $rowTochange = '';
  $(document).on('click', '.cancelBtn', function(e){
  	//Show Dialog
  	$('#cancelExtSourcePopup').dialog('open');
  	// Set var to which row being removed
  	$rowTochange = $(this).closest('tr');
  });
	$(document).on('click', '#cancelSourceBtn', function(e) {
		// Change Row status
		$rowTochange.find('td.status span').text('Removed').removeClass('down');    
    //Close dialog 
    $('#cancelExtSourcePopup').dialog('close');
    // Re-Init var
    $rowTochange = '';
  });
	
	// Confirm/Update button
	$(document).on('click', '#confirmDetailBtn', function(){
		if ($('#sourceDetailErrors').find('li:visible').length === 0) {
			$('.transferDetailWrapper').removeClass('hidden');
			if ($('#inputDetails').hasClass('hidden')) {
				$('#inputDetails').removeClass('hidden');
				$('#inputReadOnly').addClass('hidden');
			} else {
				$('#inputDetails').addClass('hidden');
				$('#inputReadOnly').removeClass('hidden');
			}
		}
		//$("#clientdetails").addClass("closed")
	});

	// Dollar formatting
  $(document).on('blur', '.dollarFormat', function() {
    this.value = parseFloat(this.value.replace(/,/g, ""))
                  .toFixed(2)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    this.value = '$' + this.value;
  });
  
  // Percentage formatting
  $(document).on('blur', '.percentageFormat', function() {
  	if ($(this).val().indexOf("%") > -1 ) {
    	this.value = this.value.substr(0,this.value.length-1) + '%';
   	} else {
   		this.value = this.value + '%';
   	}
  });


	// Phone/Digital/Wet Signature - First choice
  $(document).on('change', 'input[name=custAuth]', function() {
		//Reset Form
		$('.tiaaSendForms_wrapper, .thirdPartyForms_wrapper, .thirdParty_wrapper, .carrierForms_wrapper, .timeUntilClientReceives_wrapper, .selectSignatory_wrapper, .medallionSignature_wrapper, .howFormsAccepted_wrapper, .sendMethod_wrapper, .returnMethod_wrapper, .howFundsSent_wrapper, .followUp_wrapper, .turnAround_wrapper, .sourceContactInfo_wrapper').addClass('hidden');
		$('.authRequirementsForm').find("input[type=text], select").val("");
		$('.authRequirementsForm input[type="radio":checked]:not("input[name=custAuth]")').each(function(){
      $(this).prop('checked', false);
  	});
		// Reset Forms Handling Summary Panel
  	formsHandlingSummaryUpdate('clientAuthReqIndicator', 'TBD');
		formsHandlingSummaryUpdate('reqFormsIndicator', 'TBD');
		formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'TBD');
		formsHandlingSummaryUpdate('delMethodCarierIndicator', 'TBD');
		formsHandlingSummaryUpdate('delMethodClientIndicator', 'TBD');

		var $id = $(this).attr('id');
		authReqModel['custAuthMethodVal'] = $id;
		
		if($id == 'custAuth-phone') {
			$('.howFundsSent_wrapper').removeClass('hidden');
			
		} else {
			$('.digitalAuth').addClass('hidden');
		}

		if($id == 'custAuth-digital' || $id == 'custAuth-wetSig' ) {
			$('.carrierForms_wrapper').removeClass('hidden');
		} else {
			$('.carrierForms_wrapper').addClass('hidden');
		}
		if($id == 'custAuth-digital') {
			formsHandlingSummaryUpdate('clientAuthReqIndicator', 'DocuSign');			
		} else if($id == 'custAuth-wetSig') {
			formsHandlingSummaryUpdate('clientAuthReqIndicator', 'Wet Sign');
		} else if($id == 'custAuth-phone') {
			formsHandlingSummaryUpdate('clientAuthReqIndicator', 'Phone');
			formsHandlingSummaryUpdate('reqFormsIndicator', 'NA');
			formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'No MSG');
			formsHandlingSummaryUpdate('delMethodCarierIndicator', 'NA');
			formsHandlingSummaryUpdate('delMethodClientIndicator', 'NA');
		}
		
		closeContent2();

	});
 
  	// Carrier Forms
  	$(document).on('change', 'input[name=carrierForms2]', function(){
			var $id = $(this).attr('id'); 
			 
			 if($id == 'carrierForms-tiaa') {  
			 
			   if($("#custAuthwetSig").is(':checked')) {  
			   closeContent2(); 
			   }
			  
			 if($("#custAuthdigital").is(':checked')) {  
			  $('.timeUntilClientReceives_wrapper').addClass('hidden');
			  $('.medallionSignature_wrapper').removeClass('hidden');
			  
			 } 
			 }
			  
			if($id == 'carrierForms-both') { closeContent2();
				$('.timeUntilClientReceives_wrapper').removeClass('hidden');
				 
					if($("#custAuthdigital").is(':checked')) {  
					$('.medallionSignature_wrapper').removeClass('hidden');
					}
				 
			}
				if($id == 'carrierForms-alt') { 
					$('.timeUntilClientReceives_wrapper').removeClass('hidden');
					 if($("#custAuthwetSig").is(':checked')) {    
					 	$('.medallionSignature_wrapper').removeClass('hidden');
				 
			 closeContent2();
			 }
				}
		    if($id == 'carrierForms-tiaa') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'TIAA'); 
				//$('.medallionSignature_wrapper').removeClass('hidden');
				
			} else if($id == 'carrierForms-alt') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'Alternate Carrier');
			} else if($id == 'carrierForms-both') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'Alt Carrier & TIAA');
			}

		});	
  	// Digitial or Wet Signature path
  	// Carrier Forms
  	$(document).on('change', 'input[name=carrierForms]', function(){
			var $id = $(this).attr('id'),
					$transferType = authReqModel['transferTypeVal'];

			if($id != '') {
				 
				// Direct Rollover
				if ($transferType.indexOf('directRollover') > -1) {
					$('.thirdParty_wrapper').removeClass('hidden');
				} else {
					$('.thirdParty_wrapper').addClass('hidden');
				} 
				// Direct Transfer
				if($id != 'carrierForms-tiaa') {
					$('.timeUntilClientReceives_wrapper').removeClass('hidden');
				} else {
					$('.timeUntilClientReceives_wrapper').addClass('hidden');
				}
				$('.medallionSignature_wrapper').removeClass('hidden');
				 
			} else {
				$('.medallionSignature_wrapper').addClass('hidden');
				$('.timeUntilClientReceives_wrapper').addClass('hidden');
			}

			// Update Forms Handling Summary Panel
			if($id == 'carrierForms-tiaa') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'TIAA');
			} else if($id == 'carrierForms-alt') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'Alternate Carrier');
			} else if($id == 'carrierForms-both') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'Alt Carrier & TIAA');
			}
			
			if($id == 'carrierForms-both') { 
				$('.timeUntilClientReceives_wrapper').removeClass('hidden');
			}
			
			 
		});	
	
	  	$(document).on('keyup', 'input[id=timeUntilClientReceives]', function(){
			var $idval = $(this).val(); 
			var intRegex = /^\d+$/;
            var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;  
            if(intRegex.test($idval) || floatRegex.test($idval)) {
				if($("#custAuthdigital").is(':checked')) { 
						 $('.thirdParty_wrapper').addClass('closed');
			 $('.thirdParty_wrapper').addClass('hidden');
					}else{
				
            $('.thirdParty_wrapper').removeClass('closed');
			 $('.thirdParty_wrapper').removeClass('hidden');
					}
            }
 
		});
		 
		$(document).on('change', 'input[name=thirdPartyForms2]', function(){ 
					    if($("#thirdPartyForms-empAuth").is(':checked') || $("#thirdPartyForms-spouseWaiver").is(':checked')) {   
				        $('.medallionSignature_wrapper').removeClass('hidden'); 
					    }else{ 
							// $('.medallionSignature_wrapper').addClass('closed');
						} 
		});
		
		$(document).on('change', 'input[name=thirdParty2]', function(){
			var $id = $(this).attr('id');
			if($id == 'thirdParty-yes') {
				$('.thirdPartyForms_wrapper').removeClass('closed');
			} else {
				//$('.thirdPartyForms_wrapper').addClass('closed');
			}
		});
		 
  	// Third Party Forms Required
  	$(document).on('change', 'input[name=thirdParty]', function(){
			var $id = $(this).attr('id');
			if($id == 'thirdParty-yes') {
				$('.thirdPartyForms_wrapper').removeClass('hidden');
			} else {
				$('.thirdPartyForms_wrapper').addClass('hidden');
			}
		});
	
 
  	$(document).on('change', 'input[name=medalSignature]', function(){
			var $id = $(this).attr('id');
			if($id != '') { 
				if($id == 'medallionSignature-loa') { 
					if($("#custAuthdigital").is(':checked')) { 
						 $('.selectSignatory_wrapper').removeClass('hidden');
					}
				} 
				 
					if($id == 'medallionSignature-no') { 
					if($("#custAuthdigital").is(':checked')) { 
						$('.selectSignatory_wrapper').addClass('hidden');   
					}
					}
					 
				if($id == 'medallionSignature-yes') { 
					if($("#custAuthdigital").is(':checked')) {
						$('.howFormsAccepted_wrapper2').removeClass('hidden'); 
						$('.selectSignatory_wrapper').addClass('hidden'); 
						
						
					}
					else{
					$('.howFormsAccepted_wrapper').removeClass('hidden');
					}
				} else {
					//$('.howFormsAccepted_wrapper').addClass('hidden');
				} }
    });
	
		// Medallion
  	$(document).on('change', 'input[name=medallionSignature]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				if($id == 'medallionSignature-loa') {
					$('.selectSignatory_wrapper').removeClass('hidden');
				} else {
					$('.selectSignatory_wrapper').addClass('hidden');
				}
				if($id == 'medallionSignature-yes') {
					$('.digitalAuth').addClass('hidden');
					 
					
				}else{
					$('.digitalAuth').removeClass('hidden');
				}
				if(authReqModel['custAuthMethodVal'] == 'custAuth-wetSig') {
					$('.howFormsAccepted_wrapper .docuSignShow').removeClass('hidden');
					$('.howFormsAccepted_wrapper .wetSignShow').addClass('hidden');
				} else {
					$('.howFormsAccepted_wrapper .docuSignShow').addClass('hidden');
					$('.howFormsAccepted_wrapper .wetSignShow').removeClass('hidden');
				}
				$('.howFormsAccepted_wrapper').removeClass('hidden');
			} else {
				$('.howFormsAccepted_wrapper').addClass('hidden');
				$('.selectSignatory_wrapper').addClass('hidden');
			}
			// Update Forms Handling Summary Panel
			if($id == 'medallionSignature-yes') {
				formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'MSG Required');
			} else if($id == 'medallionSignature-no') {
				formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'No MSG');
			} else if($id == 'medallionSignature-loa') {
				formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'No MSG, but Wet LOA');
			}
		});



	// How Forms are Accepted
  	$(document).on('change', 'input[name=formsAccepted2]', function(){
			var $id = $(this).attr('id'),
					$custAuth = $('input[name=formsAccepted2]:checked').attr('id');
			 
			if ($custAuth == 'formsAccepted-original') { 
			
			$('.porder').addClass('closed');
		    $('.howFundsSent_wrapper').removeClass('hidden');
			} else { 
			}
		 
		});

		// How Forms are Accepted
  	$(document).on('change', 'input[name=formsAccepted]', function(){
			var $id = $(this).attr('id'),
					$custAuth = $('input[name=custAuth]:checked').attr('id');
					  
			if ($custAuth == 'custAuth-digital') {
				// Go to 'How will the funds be sent?'
				if($id != '') {
					$('.howFundsSent_wrapper').removeClass('hidden');
				} else {
					 $('.howFundsSent_wrapper').addClass('hidden');
				}
			} else {
				 
				// Go to 'How should TIAA send forms to the client?'
				/*if($id != '') {
					$('.sendMethod_wrapper').removeClass('hidden');
				} else {
					$('.sendMethod_wrapper').addClass('hidden');
				}*/
				
				if($id != '') {
					$('.howFundsSent_wrapper').removeClass('hidden');
				} else {
					 $('.howFundsSent_wrapper').addClass('hidden');
				} 
				  
			}
			// Update Forms Handling Summary Panel
			if($id == 'formsAccepted-original') {
				formsHandlingSummaryUpdate('delMethodCarierIndicator', 'Original Wet Signed Form');
				
			} else if($id == 'formsAccepted-copy') {
				formsHandlingSummaryUpdate('delMethodCarierIndicator', 'Copy Paper via Mail');
			} else if($id == 'formsAccepted-efax') {
				formsHandlingSummaryUpdate('delMethodCarierIndicator', 'eFax');
			}
			
				if ($id == 'formsAccepted-copy') {	 
					$('.howFundsSent_wrapper').removeClass('hidden');
				}
		});
		// How Forms are Sent
  	$(document).on('change', 'input[name=followupConversations]', function(){  
			var $id = $(this).attr('id');
			 
			if( ($id == 'followupConversations2') || ($id=='followup_Conversations') ) {
				$('.tiaaSendForms_wrapper').removeClass('hidden'); 
				$('.howFundsSent_wrapper').addClass('hidden');   
			} else {
				//$('.howFundsSent_wrapper').addClass('hidden');
			}
			
			if( ($id == 'followupConversationsOpt2') || ($id=='followup_ConversationsOpt') ) {
				$('.tiaaSendForms_wrapperOpt').removeClass('hidden'); 
				$('.howFundsSent_wrapper').addClass('hidden');  
			} else {
				//$('.howFundsSent_wrapper').addClass('hidden');
			}
			 
			if($("#custAuthdigital").is(':checked')) { 
				if($id == 'followup_Conversations') {
				$('.tiaaSendForms_wrapperOpt').removeClass('hidden');  
				$('.tiaaSendForms_wrapperOpt').removeClass('closed'); 
				$('.tiaaSendForms_wrapper').addClass('hidden');  
				
				
			    } else { 
			    } 
			}
			 
			
		});
	
	  	$(document).on('change', 'input[name=sendMethod]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.howFundsSent_wrapper').removeClass('hidden');
			} else {
				$('.howFundsSent_wrapper').addClass('hidden');
			}
		});
	
	
	
	  	$(document).on('keyup', 'input[id=turnAround]', function(){
			var $idval = $(this).val(); 
			var intRegex = /^\d+$/;
            var floatRegex = /^((\d+(\.\d *)?)|((\d*\.)?\d+))$/;  
            if(intRegex.test($idval) || floatRegex.test($idval)) {
				
				if($("#custAuthphone").is(':checked')) {
					$('.followupconversations_wrapperOpt').removeClass('closed');
			        $('.followupconversations_wrapperOpt').removeClass('hidden');
				}else{
				
            $('.followupconversations_wrapper').removeClass('closed');
			 $('.followupconversations_wrapper').removeClass('hidden');
				}
            }
 
		});
 
		// Funds Sent 
	  $(document).on('change', 'input[name=fundsSent2]', function(){
			var $id = $(this).attr('id');
			if(($id == 'checktoClient') || ($id=='checktoTiaa') || ($id=='wireTransfer')){
				$('.turnAround_wrapper').removeClass('hidden');
			} else {
				//$('.turnAround_wrapper').addClass('hidden');
			}
		});
	  

		// Phone Path
		// Funds Sent 
	  $(document).on('change', 'input[name=fundsSent]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.turnAround_wrapper').removeClass('hidden');
			} else {
				$('.turnAround_wrapper').addClass('hidden');
			}
		});

	  // Follow Up
		// $(document).on('change', 'input[name=followUp]', function(){
	 	$(document).on('keypress keydown keyup change blur', 'input[name=turnAround]', function() {
			var $id = $(this).attr('id');
			if($id != '') {
				$('.followUp_wrapper').removeClass('hidden');
			} else {
				$('.followUp_wrapper').addClass('hidden');
			}
		});

	 	// Client Delivery Option
		$(document).on('change', 'input[name=followUp]', function() {
			var $id = $(this).attr('id');
		 
			if($id != '') {
				 
						 $('.tiaaSendForms_wrapper').removeClass('hidden'); 
					 
			} else {
				$('.tiaaSendForms_wrapper').addClass('hidden');
			}	
			
			
			
		});

         $('input[type=radio][name=tiaaClientForms2]').click(function(){
		 	var id = $('input[name=tiaaClientForms2]:checked').attr('id');

		 	if(id == "tiaaClientForms-docusign"){
		 			
					if($("#custAuthwetSig").is(':checked')) {  
					$(".returnedFormsTiaa_wrapper2").removeClass("hidden"); 
					$("#releaseto_fullfillment").addClass("closed");
					$("#primaryemail3").addClass("closed"); 
					}else{
					
		 		$("#releaseto_fullfillment").removeClass("closed");
		 		$("#primaryemail3").addClass("closed"); 
				$(".returnedFormsTiaa_wrapper").removeClass("hidden");
					}
				//$("#sourceContactInfo_wrapper").addClass("hidden");
				
		 	}
		 	else if(id == "tiaaClientForms-paper"){
		 		$("#releaseto_fullfillment").removeClass("closed");
		 		$("#primaryemail3").addClass("closed"); 
                 $(".shippingmethodCarrier").removeClass("hidden");
				 $(".returnedFormsTiaa_wrapper").removeClass("hidden");
				 //$("#sourceContactInfo_wrapper").addClass("hidden");
		 	}
		 	else if(id == "tiaaClientForms-edelivery"){
		 		$("#primaryemail3").removeClass("closed");
		 		$("#releaseto_fullfillment").addClass("closed");
				$(".returnedFormsTiaa_wrapper").removeClass("hidden");
				//$("#sourceContactInfo_wrapper").addClass("hidden");
 
		 	}

		 });

		 $('input[type=radio][name=tiaaClientForms]').click(function(){
		 	var id = $('input[name=tiaaClientForms]:checked').attr('id');

		 	if(id == "tiaaClientForms-docusign"){
		 			
		 			$("#releaseto_fullfillment").addClass("closed");
		 	}
		 	else if(id == "tiaaClientForms-paper"){
		 		$("#releaseto_fullfillment").removeClass("closed");
		 		$("#primaryemail").addClass("closed");
		 		

		 	}
		 	else if(id == "tiaaClientForms-edelivery"){
		 		$("#primaryemail").removeClass("closed");
		 		$("#releaseto_fullfillment").addClass("closed");
		 	}

		 });
 
		// Forms Returned to TIAA
		$(document).on('change', 'input[name=tiaaSendForms]', function() {
			var $id = $(this).attr('id');  
			
				if($id == 'tiaaSendForms-docusign2') {
				$('.returnedFormsTiaa_wrapper2').removeClass('hidden'); 
				$("#primaryemail2").addClass("closed");  
			    } 
				
				if($id == 'tiaaSendForms-paper2') {
				$('.returnedFormsTiaa_wrapper2').addClass('hidden'); 
				$("#primaryemail2").addClass("closed");  
			    } 
				
			
			if($id == 'tiaaSendForms-docusign') {
				$('.returnedFormsTiaa_wrapper').removeClass('hidden'); 
				$("#primaryemail").addClass("closed");
				
				 
			} 
			else if($id == "tiaaSendForms-edelivery"){
				$('.returnedFormsTiaa_wrapper').addClass('hidden');
				$("#primaryemail").removeClass("closed");
				
				$('.returnedFormsTiaa_wrapper2').addClass('hidden'); 

			}
			else {
				$('.returnedFormsTiaa_wrapper').addClass('hidden');
				$("#primaryemail").addClass("closed");
			}
 
			// Update Forms Handling Summary Panel
			if($id == 'tiaaSendForms-docusign') {
				formsHandlingSummaryUpdate('delMethodClientIndicator', 'DocuSign');
			} else if($id == 'tiaaSendForms-edelivery') {
				formsHandlingSummaryUpdate('delMethodClientIndicator', 'eDelivery');
			} else if($id == 'tiaaSendForms-paper') {
				formsHandlingSummaryUpdate('delMethodClientIndicator', 'Paper Mail (Print Local)');
			}
		});
 
	// Forms Accepted
	$(document).on('change', 'input[name=formsAccepted]', function() {
		var $id = $(this).attr('id');
		if ($id == 'formsAccepted-efax') {
			$('.efaxNumber').removeClass('hidden');
		} else {
			$('.efaxNumber').addClass('hidden');
		}
 
	});
	
	$(document).on('change', 'input[name=contributionType]', function(){
		$id = $(this).attr('id');
		if($id == 'contributionType-yes') {
			$('.eftOrCheck_wrapper').removeClass('hidden');
		}else{
			$('.eftOrCheck_wrapper').addClass('hidden');
		}
	});

	$(document).on('change', 'input[name=eftOrCheck]', function(){
		var $id = $(this).attr('id');

		if ($('input[name="eftOrCheck"]:checked').length > 0) {
			$('.contributionsMadeSoFar').removeClass('hidden');
		} else {
			$('.contributionsMadeSoFar').addClass('hidden');
		}
		
		if($(this).is(':checked')) {	
			if($id == 'eftOrCheck-eft') {
				$('.contributionEFT-wrapper').removeClass('hidden');
			}
			if($id == 'eftOrCheck-check') {
				$('.contributionCheck-wrapper').removeClass('hidden');
			}
			
		} else {
			if($id == 'eftOrCheck-eft') {
				$('.contributionEFT-wrapper').addClass('hidden');
			}
			if($id == 'eftOrCheck-check') {
				$('.contributionCheck-wrapper').addClass('hidden');
			}
		}
	});

	$(document).on('change', 'input[name=jointAccount]', function(){
		$id = $(this).attr('id');
		if($id == 'jointAccount-yes') {
			$('.joint_wrapper').removeClass('hidden');
			$('.bankName_wrapper').css('marginTop', '0px');
		}else{
			$('.joint_wrapper').addClass('hidden');
			$('.bankName_wrapper').css('marginTop', '17px');
		}
	});
	

	$(document).on('change', 'input[id=eftType-oneTime]', function(){
		if($(this).is(':checked')) {
			$('.oneTime_wrapper').removeClass('hidden');
		} else {
			$('.oneTime_wrapper').addClass('hidden');
		}
	});
	$(document).on('change', 'input[id=eftType-recurring]', function() {
		if($(this).is(':checked')) {
			$('.recurring_wrapper').removeClass('hidden');
			$('.totalFutureContributions_wrapper').removeClass('hidden');
		} else {
			$('.recurring_wrapper').addClass('hidden');
			$('.totalFutureContributions_wrapper').addClass('hidden');
		}
	});
	
	$(document).on('change', 'input[id=eftType-oneTime], input[id=eftType-recurring]', function(){
		if($(this).is(':checked')) {
			$('.totalContributions_wrapper').removeClass('hidden');
		} else {
			$('.totalContributions_wrapper').addClass('hidden');
		}
	});

	$(document).on('change', 'input[name=eftType]', function() {
		var $atLeastOneIsChecked = $('input[name="eftType"]:checked').length > 0;		
		if($atLeastOneIsChecked) {	
			$('.bankInformation').removeClass('hidden');
		}	else {
			$('.bankInformation').addClass('hidden');
		}
	});
	
	$(document).on('click focus blur change keyup', 'input[name=reccuringStart], #recurringMonthly', function() {
		var $monthly = $('#recurringMonthly').val(),
				$recurringDate = $(this).val();

  	if ($monthly != '' &&  $recurringDate != '') {
    	$('.recurringMonthly_wrapper').removeClass('hidden');
   	} else {
   		$('.recurringMonthly_wrapper').addClass('hidden');
   	}
  });
 
		$('#editOption').on('click',function(e){ 
			   $(".editClose").removeClass('closed'); 
			   $('.editBtn').addClass('closed'); 
			   
			   $(".msgyesOrno").removeClass('closed');
			   $(".closeeditVal").addClass('closed');
			    
			   $("#inputdetailsnonGold :input").removeAttr('disabled','disabled');
			   $(".addressClose").removeClass('closed');
			   $(".addressOpen").addClass('closed');
				
			   $(".maturitydateCd").removeClass('hidden');
			   $(".maturityDate").addClass('closed');
				 
			   $(".closeeditVal").addClass('closed');
		       $(".classAll").removeClass('closed');
		});

	$('#saveUpdates').on('click',function(e){
						 
		var flag=0;
		$("#nongolderrmessage").html('');
		if($("#addressLine1").val() == ''){
			$("#nongolderrmessage").append("<li>Address Line1 field empty</li>");
			$("#addressLine1").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag =1;
		}
		else{
			$("#addressLine1").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}

		if($("#city").val() == ''){
			$("#nongolderrmessage").append("<li>City field empty</li>");
			$("#city").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag=1;
		}	
		else{
			$("#city").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}

		if($("#state_Input").val() == ''){
			$("#nongolderrmessage").append("<li>State field empty</li>");
			$("#state_Input").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag=1;
		}
		else{
			$("#state_Input").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}		

		if($("#zipCode").val() == ''){
			$("#nongolderrmessage").append("<li>zipcode  field empty</li>");
			$("#zipCode").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag=1;
		}	
		else{
			$("#zipCode").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}

		if($("#phone").val() == ''){
			$("#nongolderrmessage").append("<li>Phone field empty</li>");
			$("#phone").closest("div.lblFieldPairV").addClass("alertHighlight");
			flag=1;
		}	
		else{
			$("#phone").closest("div.lblFieldPairV").removeClass("alertHighlight");
		}	


		if(flag == 1){
			$("#validationErrormsg").removeClass("hidden");
			$("#validationErrormsg").addClass("visible");
		}
		else{
			$("#validationErrormsg").addClass("hidden");
			$("#validationErrormsg").removeClass("visible");
		}
		 
		var transfer =$('input[name=transfer]:checked').val();  
		var ircCode=$("#accountType").val();
		var account_Number=$("#account_Number").val();
		var amount=$("#amount").val(); 
		var lIquid =$('input[name=lIquid]:checked').val();  
		var alloc =$('input[name=alloc]:checked').val(); 
		var depositcds =$('input[name=depositcds]:checked').val(); 
		var msgrequired =$('input[name=msgRequired]:checked').val();  
		var waitfor_Maturity =$('input[name=cashoutCD]:checked').val(); 
		var carrierName=$("#carrierName").val(); 
		var addressLine1=$("#addressLine1").val();
		var addressLine2=$("#addressLine2").val(); 
		var city=$("#city").val();
		var state_Input=$("#state_Input").val(); 
		var zipCode=$("#zipCode").val();
		var phone=$("#phone").val(); 
		
	    var cd_maturityDate=$("#cd_maturityDate").val(); 
		var cd_Day=$("#cd_Day").val();
		var cd_Year=$("#cd_Year").val();
		 
	  if(  $( "#validationErrormsg" ).hasClass( "visible" )  ){
		    $(".closeeditVal").addClass('closed');
		    $(".classAll").removeClass('closed');
	   }else{
		  
		  	$("#addressLine1val").html(addressLine1);
		    $("#addressLine2val").html(addressLine2);
		    $("#cityval").html(city);
		    $("#state_Inputval").html(state_Input);
		    $("#zipCodeval").html(zipCode);
		    $("#phoneval").html(phone);
		  
		$("#msgRequiredval").html(msgrequired);
		
		$(".closeeditVal").removeClass('closed');
		$(".classAll").addClass('closed');
		 
		$(".msgyesOrno").addClass('closed');
		$(".addressClose").addClass('closed'); 
		$(".addressOpen").removeClass('closed');
		 
		 $(".maturitydateCd").addClass('hidden');
		 $(".maturityDate").removeClass('closed');
		  
		 $("#tiaaTransferval").html(transfer);
		 $("#irc_Codeval").html(ircCode);
		 $("#account_Numberval").html(account_Number);
		 $("#amountval").html(amount);
		 
		 $("#liquidation_Instructionsval").html(lIquid);
		 $("#allocationInstructionsval").html(alloc);
		 
		 $("#depositcdval").html(depositcds);
		 $("#waitfor_Maturity").html(waitfor_Maturity);
		 
		 $("#maturity_date").html(cd_maturityDate+"=="+cd_Day+"=="+cd_Year); 
		 $("#carrierNameval").html(carrierName);
		 
		 var waitformaturity2 =$('input[name=cashoutCD]:checked').val();
		 
		 if(waitformaturity2=="Immediately"){
			 
			 $(".maturityDate").addClass('closed');
			 $(".maturitydateCd").addClass('closed');
			  
		 }else{
			 $(".maturityDate").removeClass('closed');
			 $(".maturitydateCd").removeClass('closed');
		 } 
	     }
		   
		    
		if( (addressLine1=="") || (city=="") || (state_Input=="") || (zipCode=="") || (phone=="") ){
		$("#validationErrormsg").addClass('visible'); 
		 
		}else{  
	    $(".editBtn").removeClass('closed');
		$(".editClose").addClass('closed');  
		$("#inputdetailsnonGold :input").attr('disabled','disabled'); 
		$("#validationErrormsg").removeClass('visible');
		}
		
    });

});
		

function closeContent(){

		$(".maturitydateCd").addClass('hidden');
		
		$(".timeUntilClientReceives_wrapper").addClass('hidden');
		$(".thirdParty_wrapper").addClass('closed');
		$(".thirdPartyForms_wrapper").addClass('closed');
		$(".medallionSignature_wrapper").addClass('hidden');
		
		$(".selectSignatory_wrapper").addClass('hidden');
		$(".howFormsAccepted_wrapper2").addClass('hidden');
		$(".howFormsAccepted_wrapper").addClass('hidden');
		$(".sendMethod_wrapper").addClass('hidden');
		
		$(".turnAround_wrapper").addClass('hidden');
		$(".followupconversations_wrapper").addClass('hidden');
		$(".followupconversations_wrapperOpt").addClass('hidden');
		$(".tiaaSendForms_wrapperOpt").addClass('hidden');
		$(".followUp_wrapper").addClass('closed');
		
		$(".tiaaSendForms_wrapper").addClass('hidden');
		$("#primaryemail3").addClass('closed');
		$(".returnedFormsTiaa_wrapper2").addClass('hidden');
		$("#primaryemail2").addClass('closed');
		$(".sourceContactInfo_wrapper").addClass('hidden');
		
		$(".radiobtn").find("input:radio").prop("checked", false);
		$('.radiobtn input[type="text"]').val('');
		
		$(".returnedFormsTiaa_wrapper").addClass('hidden');
		

}
		
function closeContent2(){

		$(".maturitydateCd").addClass('hidden');
		
		$(".timeUntilClientReceives_wrapper").removeClass('hidden');
		$(".thirdParty_wrapper").addClass('closed');
		$(".thirdPartyForms_wrapper").addClass('closed');
		$(".medallionSignature_wrapper").addClass('hidden');
		
		$(".selectSignatory_wrapper").addClass('hidden');
		$(".howFormsAccepted_wrapper2").addClass('hidden');
		$(".howFormsAccepted_wrapper").addClass('hidden');
		$(".sendMethod_wrapper").addClass('hidden');
		
		$(".turnAround_wrapper").addClass('hidden');
		$(".followupconversations_wrapper").addClass('hidden');
		$(".followupconversations_wrapperOpt").addClass('hidden');
		$(".tiaaSendForms_wrapperOpt").addClass('hidden');
		$(".followUp_wrapper").addClass('closed');
		
		$(".tiaaSendForms_wrapper").addClass('hidden');
		$("#primaryemail3").addClass('closed');
		$(".returnedFormsTiaa_wrapper2").addClass('hidden');
		$("#primaryemail2").addClass('closed');
		$(".sourceContactInfo_wrapper").addClass('hidden');
        $(".returnedFormsTiaa_wrapper").addClass('hidden');
 
}
		
		
		
		 