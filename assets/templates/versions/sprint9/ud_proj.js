// OneIRA Project-specific JS
$(document).on('bodyContLoaded', function(){

	// Multi Select
	$.getScript('assets/js/multi_select.js', function(){
		$('.multiSel').multipleSelect();
	});		

	// START - Forms Validation for Transfer Details
	function requiredFieldFail(element, wrapper) {
		var id = element.attr('id'),
				errorWrapper = wrapper,
		 		elementParent = element.closest('.lblFieldPairV, .lblFieldPair, tr'),
				elementError = $("#"+id+"-err");
		
		// Highlight field and show error
		elementParent.addClass('alertHighlight');
		elementError.removeClass('hidden');
		
		// Show Error Box
		$(errorWrapper).addClass('visible');
		errorBox($(errorWrapper));
	};

	function requiredFieldSuccess(element, wrapper) {
		var id =element.attr('id'),
				errorWrapper = wrapper,
			 	elementParent = element.closest('.lblFieldPairV, .lblFieldPair, tr'),
				elementError = $("#"+id+"-err");

		// Remove highlight from field and show error
		elementParent.removeClass('alertHighlight');
		elementError.addClass('hidden');
		
		// Hide Error Box
		errorBox($(errorWrapper));
	};

	function errorBox(elem) {
		if (elem.find('li:visible').length === 0) {
		 	elem.removeClass('visible');
		} else if (elem.find('li:visible').length != 0) {
			elem.addClass('visible');
		}
	};

	function phoneFormatCheck(value) {
    value = $.trim(value).replace(/\D/g, '');

    if (value.substring(0, 1) == '1') {
        value = value.substring(1);
    }

    if (value.length == 10) {
        return value;
    }
    return false;
	}

	// CD Selection on Funding Details
	var $cdSelect = false;
	$(document).on('change', '#investmentTypeDetail', function(){
		var $val = $(this).val();
		if ($val == 'cd') {
			$cdSelect = true;
		} else {
			$cdSelect = false;
		}
		
		if ($cdSelect) {
			$('.nonCDContent').addClass('hidden');
			$('.CDContent').removeClass('hidden');
		}
	});
	
	$(document).on('change', '#transferType', function() {
		var $val = $(this).val();
		console.log($val);
		if ($val.indexOf('directTransfer')) {
			$('.accountTypeDirect').addClass('hidden');
			$('.accountTypeOther').removeClass('hidden');
		} else {
			$('.accountTypeDirect').removeClass('hidden');
			$('.accountTypeOther').addClass('hidden');
		}
	});

	// IGO/NIGO
	$(document).on('change', 'input[name=custAuthRO]', function() {
		$('.authRequirementsROForm').addClass('hidden');
		$('.authRequirementsForm').removeClass('hidden');
	});

	// IGO Popup
	$(document).on('click', '#igoSubmitBtn', function() {
		$('.fundingDefaultHeader').addClass('hidden');
		$('.fundingReq_wrapper').removeClass('hidden');
		$('#igoPopupBtn').addClass('hidden');
		$('#igoLaoBtn').removeClass('hidden');


	});

	// LOA Show Letter Link
	$(document).on('change', '#letterOfAcceptance', function() {
		$('.loaDoc_wrapper').removeClass('hidden');
	});

	// Save & Exit Check
	$(document).on('click', '#saveExitBtn', function() {
		if (!$('input[name=loaPrinted]').is(':checked')) {
			$('.loaAlertText').removeClass('hidden');
		} else {
			$('.loaAlertText').addClass('hidden');
		}
	});

	// Other Authorizations
	$(document).on('change', 'input[name=otherAuth]', function() {
		var $id = $(this).attr('id');

		if ($id === 'otherAuth-yes') {
			$('#otherInput').removeClass('hidden');
		} else {
			$('#otherInput').addClass('hidden');
		}
	});



	// Check that Maturity Date selected is not beyond 180 days
	$(document).on('change', '#maturityDate', function() {
			var $selectedDate = $(this).datepicker('getDate'),
					fullDate = new Date(),
					diffDate = ($selectedDate - fullDate)/1000/60/60/24;

			if (diffDate > 180) {
				$("#popupMaturity").dialog('open');
			}			
	});
	
	// Populate Bank Accoutn on File - SIM Code, NOT for DEV
	$(document).on('change', '#acctOnFile', function(){
		$('#acctType-checking').prop("checked", true);
		$('#acctHolderFName').val('Penelope');
		$('#acctHolderMName').val('');
		$('#acctHolderLName').val('Pension');
		$('#bankRoutingNumber').val('000000186');
		$('#bankAccountNumber').val('000000529');
	});


	// Auto opening/closing panels for Funding Edit Details
	$(document).on('click', '.transferDetailsPanelBtn', function() {
		$(document).find('a#transferDetailsPanel').trigger('click');
		$(document).find('a#authRequirementsPanel').trigger('click');
	});
	$(document).on('click', '.authRequirementsPanelBtn', function() {
		$(document).find('a#authRequirementsPanel').trigger('click');
		$(document).find('a#handlingInstructionsPanel').trigger('click');
	});


	$(document).on('click', '#continueDetailBtn', function(e) {
		e.stopPropagation();
		errorWrapper = ('#' + $('#tDetailsAlerts').attr('id'));
		$(".transferDetailWrapper").find('[data-required=true]').each(function() {
			var element = $(this),
					elementVal = $.trim(element.val());
			
			if (element.is(':hidden')) { 
				requiredFieldSuccess(element, errorWrapper);
			} else {
				if (element.is(':checkbox') || element.is(':radio')) { //If Radio or Checkbox
					if (!$(element).is(":checked")) {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}
				} else { // All other inputs
					if (elementVal == 0 || typeof elementVal == "undefined" || elementVal == "") {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}	
				}
			}
		});
		return false;
	});

	$(document).on('click', '#confirmDetailBtn', function(e) {
		e.stopPropagation();
		errorWrapper = ('#' + $('#sourceDetailErrors').attr('id'));
		$("#inputDetails").find('[data-required=true]').each(function() {
			var element = $(this),
					elementVal = $.trim(element.val());
			
			if (element.is(':hidden')) { 
				requiredFieldSuccess(element, errorWrapper);
			} else {
				if (element.is(':checkbox') || element.is(':radio')) { //If Radio or Checkbox
					if (!$(element).is(":checked")) {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}
				} else { // All other inputs
					if (elementVal == 0 || typeof elementVal == "undefined" || elementVal == "") {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}	
				}
			}
		});
		return false;
	});
	// END - Forms Validation for Transfer Details

	
	$(document).on('click','input[name=wtradio]',function(){
		if($('#wtradiono').is(':checked')) { 
		 	$(".wtradionoDiv").removeClass('hidden');
		}
		if($('#wtradioyes').is(':checked')) { 
		 	$(".wtradionoDiv").addClass('hidden');
		}
	});
	$(document).on('click','input[name=allocationChoice]',function(){
		if($('#allocationChoiceNo').is(':checked')) { 
		 	$(".contributionsMadeSoFar").removeClass('hidden');
		} else {
		 	$(".contributionsMadeSoFar").addClass('hidden');
		}
	});
	


	// Show/hide for Create New funding Request
	$(".createNewFundingRequest").click(function() {
	    $(".createNewFundingRequest").addClass("hidden");
	    $(".accountToFund-wrapper").removeClass("hidden");
	}); 

	// Show/Hide Funding Sources for Fund an Account
	var $origVal = '';
	var $newVal = '';
	$('.addFundingToConfirmBtn').on('click', function() {
		var $val = $("#addFundingTo").val();
		if ($val != "") {
			$('.panels').removeClass("hidden");
			$(".alertText").addClass("hidden");
			$("#addFundingTo").parent().removeClass("alertHighlight");
			$(".addFundingToConfirmBtn").addClass("hidden");
			$("#addFundingTo option[value='']").remove();
			$origVal = $("#addFundingTo").val();
			$("#addFundingTo").attr("id", "addFundingToChange");
			$('.orchestrationId_wrapper').removeClass('hidden');
		} else {
			$('.panels').addClass("hidden");
			$(".alertText").removeClass("hidden");
			$("#addFundingTo").parent().addClass("alertHighlight");
		}
	});

	$(document).on('change', '#addFundingToChange', function(){
		$newVal = $("#addFundingToChange").val();
		if ($newVal != $origVal) {
			$('#eraseRequest').dialog('open');
		}
	});
	$('#yesChangeBtn').on('click', function() {
		$origVal = $("#addFundingToChange").val();
		$('#eraseRequest').dialog('close');
	});
	$('#noChangeBtn').on('click', function() {
		$("#addFundingToChange").val($origVal);
		$('#eraseRequest').dialog('close');
	});

	// Show Bank Info
	$(document).on('change', 'input[name="eftProvide"]', function() {
	  var $id = $(this).attr('id');
		if($id == 'eftProvide-phone') {
			$('.bankInfo_wrapper').removeClass('hidden');
	  } else {
	  	$('.bankInfo_wrapper').addClass('hidden');
	  }
	});

	// Show hide External rollovers
	$("input[name='external-rollovers']").click(function() {
	  var $id = $(this).attr("id");
		if($id == "external-rollovers-yes") {
			$('.extSources-wrapper').removeClass('hidden');
	  } else {
	  	$('.extSources-wrapper').addClass('hidden');
	  }
	});

	// Search Type, on change populate Business Units Dropdown
  $('#companyName').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('.otherCompany-wrapper').removeClass('hidden');
    } else {
    	$('.otherCompany-wrapper').addClass('hidden');
    }
  });

  // Search Type, on change populate Business Units Dropdown
  $('#companyNameDetail').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('#otherCompany').removeClass('hidden');
    	$('#companyNameDetail').removeClass('inputxlg').addClass('inputs');
    } else {
    	$('#otherCompany').addClass('hidden');
    	$('#companyNameDetail').addClass('inputxlg').removeClass('inputs');
    }
  });

  // Search Type, on change populate Business Units Dropdown
  $('#productNameDetail').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('#otherProduct').removeClass('hidden');
    	$('#productNameDetail').removeClass('inputxlg').addClass('inputs');
    } else {
    	$('#otherProduct').addClass('hidden');
    	$('#productNameDetail').addClass('inputxlg').removeClass('inputs');
    }
  });

	
	// Add External Sources to Table
	$(document).on('click', '#addBtn', function(e) {
		var $companyName = $('#companyName').val(),
				$otherCompany = $('#otherCompany').val(),
				$company = ($companyName == "Other") ? $otherCompany : $companyName,
				$transferAmount = $('#transferAmount').val(),
				row = '<tr>' +
								'<td>' + $company + '</td>' +
                '<td>' + $transferAmount + '</td>' +
                '<td></td>' +
                '<td></td>' +
                '<td>92Z1776H21</td>' +
                '<td class="status"><span class="down">Incomplete</span></td>' +
                '<td>' +
                  '<a href="edit_funding_detail.html" class="editBtn">Edit</a>' +
                  '<span class="dim pls prs">|</span>' +
                  '<a href="#cancelExtSourcePopup" class="cancelBtn">Remove</a>' +
                '</td>' +
              '</tr>';
		// Initialize Table & Form
    $('.noDataToDisplay').remove();
    // Add collected data
    $('#tbl_extsources0 tbody').append(row);
    // Close dialog
    $('#addExternalsourcePopup').dialog('close');
    // Clear Form for next use
    $(this).closest('form').find("input[type=text], select").val("");
  });

  

// // Begin Add/Remove Rows for Beneficiaries

//   // Remove External Sources from table
//   var $rowToRemove = '';
//   $(document).on('click', '.cancelBtn', function(e){
//   	//Show Dialog
//   	$('#cancelExtSourcePopup').dialog('open');
//   	// Set var to which row being removed
//   	$rowToRemove = $(this).closest('tr');
//   });


// 	$(document).on('click', '#cancelSourceBtn', function(e) {
// 		// Remove row designated by var
// 		$rowToRemove.remove();
// 		// Default placeholder row
// 		var row = '<tr>' +
//               	'<td colspan="7" class="noDataToDisplay">No data to display</td>' +
//               '</tr>';
//     // If table is empty, replace with placeholder row
//     if ($('#tbl_extsources0 tbody tr').children().length <= 1) {
//     	$('#tbl_extsources0 tbody').append(row);
//     }
//     //Close dialog 
//     $('#cancelExtSourcePopup').dialog('close');
//     // Re-Init var
//     $rowToRemove = '';
//   });
// // Ended Add/Remove Rows for Beneficiaries
  




  // Set External Sources to Canceled
  var $rowTochange = '';
  $(document).on('click', '.cancelBtn', function(e){
  	//Show Dialog
  	$('#cancelExtSourcePopup').dialog('open');
  	// Set var to which row being removed
  	$rowTochange = $(this).closest('tr');
  });
	$(document).on('click', '#cancelSourceBtn', function(e) {
		// Change Row status
		$rowTochange.find('td.status span').text('Removed').removeClass('down');    
    //Close dialog 
    $('#cancelExtSourcePopup').dialog('close');
    // Re-Init var
    $rowTochange = '';
  });
	
	// Confirm/Update button
	$(document).on('click', '#confirmDetailBtn', function(){
		if ($('#sourceDetailErrors').find('li:visible').length === 0) {
			$('.transferDetailWrapper').removeClass('hidden');
			if ($('#inputDetails').hasClass('hidden')) {
				$('#inputDetails').removeClass('hidden');
				$('#inputReadOnly').addClass('hidden');
			} else {
				$('#inputDetails').addClass('hidden');
				$('#inputReadOnly').removeClass('hidden');
			}
		}
	});

	// Dollar formatting
  $(document).on('blur', '.dollarFormat', function() {
    this.value = parseFloat(this.value.replace(/,/g, ""))
                  .toFixed(2)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    this.value = '$' + this.value;
  });
  
  // Percentage formatting
  $(document).on('blur', '.percentageFormat', function() {
  	if ($(this).val().indexOf("%") > -1 ) {
    	this.value = this.value.substr(0,this.value.length-1) + '%';
   	} else {
   		this.value = this.value + '%';
   	}
  });


	// Phone/Digital/Wet Signature - First choice
  $(document).on('change', 'input[name=custAuth]', function() {
		//Reset Form
		$('.carrierForms_wrapper, .medallionSignature_wrapper, .howFormsAccepted_wrapper, .sendMethod_wrapper, .returnMethod_wrapper, .howFundsSent_wrapper, .followUp_wrapper, .turnAround_wrapper, .sourceContactInfo_wrapper').addClass('hidden');
		$('.authRequirementsForm').find("input[type=text], select").val("");
		$('.authRequirementsForm input[type="radio":checked]:not("input[name=custAuth]")').each(function(){
      $(this).prop('checked', false);
  	});

		var $id = $(this).attr('id');
		if($id == 'custAuth-phone') {
			$('.howFundsSent_wrapper').removeClass('hidden');
		} else {
			$('.digitalAuth').addClass('hidden');
		}

		if($id == 'custAuth-digital' || $id == 'custAuth-wetSig' ) {
			$('.carrierForms_wrapper').removeClass('hidden');
		} else {
			$('.carrierForms_wrapper').addClass('hidden');
		}
	});

  	// Digitial or Wet Signature path
  	// Carrier Forms
  	$(document).on('change', 'input[name=carrierForms]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.medallionSignature_wrapper').removeClass('hidden');
			} else {
				$('.medallionSignature_wrapper').addClass('hidden');
			}
		});
		// Medallion
  	$(document).on('change', 'input[name=medallionSignature]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.howFormsAccepted_wrapper').removeClass('hidden');
			} else {
				$('.howFormsAccepted_wrapper').addClass('hidden');
			}
		});
		// How Forms are Accepted
  	$(document).on('change', 'input[name=formsAccepted]', function(){
			var $id = $(this).attr('id'),
					$custAuth = $('input[name=custAuth]:checked').attr('id');
			
			
				if ($custAuth == 'custAuth-digital') {
					// Go to 'How will the funds be sent?'
					if($id != '') {
						$('.howFundsSent_wrapper').removeClass('hidden');
					} else {
						$('.howFundsSent_wrapper').addClass('hidden');
					}
				} else {
					// Go to 'How should TIAA send forms to the client?'
					if($id != '') {
						$('.sendMethod_wrapper').removeClass('hidden');
					} else {
						$('.sendMethod_wrapper').addClass('hidden');
					}
				}

		});
		// How Forms are Sent
  	$(document).on('change', 'input[name=sendMethod]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.howFundsSent_wrapper').removeClass('hidden');
			} else {
				$('.howFundsSent_wrapper').addClass('hidden');
			}
		});

		// Return Method
  // 	$(document).on('change', 'input[name=returnMethod]', function(){
		// 	var $id = $(this).attr('id');
		// 	if($id != '') {
		// 		$('.howFundsSent_wrapper').removeClass('hidden');
		// 	} else {
		// 		$('.howFundsSent_wrapper').addClass('hidden');
		// 	}
		// });



		// Phone Path
		// Funds Sent 
	  $(document).on('change', 'input[name=fundsSent]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.followUp_wrapper').removeClass('hidden');
			} else {
				$('.followUp_wrapper').addClass('hidden');
			}
		});
		// Follow Up
	  $(document).on('change', 'input[name=followUp]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.turnAround_wrapper').removeClass('hidden');
			} else {
				$('.turnAround_wrapper').addClass('hidden');
			}
		});
		// Source Contact Information
	  $(document).on('keypress keydown keyup change focus blur', 'input[name=turnAround]', function() {
			var $val = $(this).val(),
					$custAuth = $('input[name=custAuth]:checked').attr('id');

			if($val != '') { 
				if ($custAuth != 'custAuth-phone') {
					$('.sourceContactInfo_wrapper').removeClass('hidden');
				} else {
					$('.sourceContactInfo_wrapper').addClass('hidden');
				}
			}
		});


	// Medallion Signature/ eFax
  $(document).on('change', 'input[name=medallionSignature]', function(){
		var $id = $(this).attr('id');
		if($id == 'medallionSignature-yes') {
			$('.digitalAuth').addClass('hidden');
		}else{
			$('.digitalAuth').removeClass('hidden');
		}
	});

	// Forms Accepted
	$(document).on('change', 'input[name=formsAccepted]', function() {
		var $id = $(this).attr('id');
		if ($id == 'formsAccepted-efax') {
			$('.efaxNumber').removeClass('hidden');
		} else {
			$('.efaxNumber').addClass('hidden');
		}

		// if ($id == 'formsAccepted-efax' || $id == 'formsAccepted-copy') {
		// 	$('.returnMethod_wrapper').removeClass('hidden');
		// } else {
		// 	$('.returnMethod_wrapper').addClass('hidden');
		// }
	});
	
	$(document).on('change', 'input[name=contributionType]', function(){
		$id = $(this).attr('id');
		if($id == 'contributionType-yes') {
			$('.eftOrCheck_wrapper').removeClass('hidden');
		}else{
			$('.eftOrCheck_wrapper').addClass('hidden');
		}
	});

	$(document).on('change', 'input[name=eftOrCheck]', function(){
		var $id = $(this).attr('id');

		if ($('input[name="eftOrCheck"]:checked').length > 0) {
			$('.contributionsMadeSoFar').removeClass('hidden');
		} else {
			$('.contributionsMadeSoFar').addClass('hidden');
		}
		
		if($(this).is(':checked')) {	
			if($id == 'eftOrCheck-eft') {
				$('.contributionEFT-wrapper').removeClass('hidden');
			}
			if($id == 'eftOrCheck-check') {
				$('.contributionCheck-wrapper').removeClass('hidden');
			}
			
		} else {
			if($id == 'eftOrCheck-eft') {
				$('.contributionEFT-wrapper').addClass('hidden');
			}
			if($id == 'eftOrCheck-check') {
				$('.contributionCheck-wrapper').addClass('hidden');
			}
		}
	});

	$(document).on('change', 'input[name=jointAccount]', function(){
		$id = $(this).attr('id');
		if($id == 'jointAccount-yes') {
			$('.joint_wrapper').removeClass('hidden');
			$('.bankName_wrapper').css('marginTop', '0px');
		}else{
			$('.joint_wrapper').addClass('hidden');
			$('.bankName_wrapper').css('marginTop', '17px');
		}
	});
	

	$(document).on('change', 'input[id=eftType-oneTime]', function(){
		if($(this).is(':checked')) {
			$('.oneTime_wrapper').removeClass('hidden');
		} else {
			$('.oneTime_wrapper').addClass('hidden');
		}
	});
	$(document).on('change', 'input[id=eftType-recurring]', function() {
		if($(this).is(':checked')) {
			$('.recurring_wrapper').removeClass('hidden');
			$('.totalFutureContributions_wrapper').removeClass('hidden');
		} else {
			$('.recurring_wrapper').addClass('hidden');
			$('.totalFutureContributions_wrapper').addClass('hidden');
		}
	});
	
	$(document).on('change', 'input[id=eftType-oneTime], input[id=eftType-recurring]', function(){
		if($(this).is(':checked')) {
			$('.totalContributions_wrapper').removeClass('hidden');
		} else {
			$('.totalContributions_wrapper').addClass('hidden');
		}
	});

	$(document).on('change', 'input[name=eftType]', function() {
		var $atLeastOneIsChecked = $('input[name="eftType"]:checked').length > 0;		
		if($atLeastOneIsChecked) {	
			$('.bankInformation').removeClass('hidden');
		}	else {
			$('.bankInformation').addClass('hidden');
		}
	});
	
	$(document).on('click focus blur change keyup', 'input[name=reccuringStart], #recurringMonthly', function() {
		var $monthly = $('#recurringMonthly').val(),
				$recurringDate = $(this).val();

  	if ($monthly != '' &&  $recurringDate != '') {
    	$('.recurringMonthly_wrapper').removeClass('hidden');
   	} else {
   		$('.recurringMonthly_wrapper').addClass('hidden');
   	}
  });


});