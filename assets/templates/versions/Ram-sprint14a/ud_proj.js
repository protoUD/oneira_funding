// OneIRA Project-specific JS
$(document).on('bodyContLoaded', function(){

	// Multi Select
	$.getScript('assets/js/multi_select.js', function(){
		$('.multiSel').multipleSelect();
	});	
	var $authReqPanel   = $('#authReqPanel'),
		$handleInsPanel = $('#handleInsPanel');
	// Transfer type functionality 
	$(document).on('click','#transferTypeCD',function(){ 
		var $options	 = $('#accountTypeCD').find('option[value="Employer Sponsored Plan"], option[value="Keogh"]'),
			$allocFund   = $('.chooseFundAlloc');
		if($(this).val() === 'directTransferCash') {
			$allocFund.removeClass('closed');
			$options.addClass('closed');
		}else{
			$allocFund.addClass('closed');
			$options.removeClass('closed');
		}	
	});
	//Funding source--> Edit funding --> funds allocated functionality
	$(document).on('click','input[name=chooseFunds]',function(){
		var $opBox = $('.opBox');
		($(this).attr('id') === 'fundsAlloc2') ? $opBox.removeClass('closed') : $opBox.addClass('closed');
	});
	//Adding fund Details button functionality
	$(document).on('click','#addFundBtn',function(e){
		e.preventDefault();
		window.open('fund_an_account.html?preview','_self')
	});
	
	var urlN = location.href.substring(location.href.lastIndexOf('?') + 1);
	if(urlN === 'preview'){
		$('#tbl_extsources0').find('.dataRow, .noDataRow').toggleClass('closed');
		$('.transferDetailWrapper, .extSources-wrapper').removeClass('hidden');
		$('#external-rollovers-yes').attr('checked',true);
		$('#addFundingTo option[value=11104837]').attr('selected','selected');
	}
	
	function GetURLParameter(sParam) {               
    var sPageURL = window.location.search.substring(1),
		sURLVariables = sPageURL.split('&');	
		for (var i = 0; i < sURLVariables.length; i++){                         
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam){                         
				return sParameterName[1];
			}
		}
	}
	
	var $adrSrh =  $('.adrSrh'),
		$nonadrSrh = $('.nonadrSrh');
	
	if (GetURLParameter("address")=="true"){
		$adrSrh.add($authReqPanel).add($handleInsPanel).removeClass('closed');
		$nonadrSrh.addClass('closed');
	}

	// Dollar formatting
  $(document).on('blur', '.dollarFormat', function(){
    this.value = parseFloat(this.value.replace('$','').replace(/,/g, ""))
                  .toFixed(2)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    this.value = this.value;
  });
  
  // Percentage formatting
  $(document).on('blur', '.percentageFormat', function() {
  	if ($(this).val().indexOf("%") > -1 ) {
    	this.value = this.value.substr(0,this.value.length-1) + '%';
   	} else {
   		this.value = this.value + '%';
   	}
  });

	
	/*==v  Authorization Requirements Flow v==*/
	var authReqModel = {
		'transferTypeVal': '',
		'custAuthMethodVal': ''
	};

	//Update the Forms Handling Summary
	function formsHandlingSummaryUpdate(ind2Update, indVal) {
			var $elem = $('.' + ind2Update),
					$elemNewVal = indVal;
			
			$elem.text(indVal);
	};
	/*==^  Authorization Requirements Flow ^==*/




	// Transfer Total Table 
	$(document).on('change', 'input[name=existingInvestment]', function() {
		var $transferTotal = 0;

		$("input[name=existingInvestment]").each(function() {
			var $thisValue = ($(this).val() != '') ? parseFloat($(this).val().replace('$','').replace(',','')) : 0;
    	$transferTotal = parseFloat($transferTotal) + parseFloat($thisValue);

    	var currBal = parseFloat($(this).closest('td').prev('td').text().replace('$','').replace(',','')),
    			percentageOfTotal = $thisValue / currBal * 100;
    	$(this).closest('td').next('td').find('input[name=percentOfTotal]').val(percentageOfTotal.toFixed(2) + '%');
		});
		
		$('#transferTotal').text($transferTotal);
		formatCurrencyText('#transferTotal');
	});


  // Format Currency to Text
  function formatCurrencyText(element) {
  	var val = $(element).text();
  	val = parseFloat(val.replace(/,/g, ""))
                  .toFixed(2)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    val = '$' + val;
    $(element).text(val);
  };
  // Format Currency to Input Value
  function formatCurrencyValue(element){
  	var val = $(element).val();
  	val = parseFloat(val.replace(/,/g, ""))
                  .toFixed(2)
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    val = '$' + val;
    $(element).val(val);
  };

	// Show Selected Internal Popups
	$(document).on('click','.showSelectIntContracts', function(e) {
		e.preventDefault();
		$(this).closest('.popup').dialog('close');
		$('.selInContracts').removeClass('hidden');
	});

	//Remove row
	$(document).on('click','.delRow', function(e) {
		$(this).closest('tr').remove();
	});


	// START - Forms Validation for Transfer Details
	function requiredFieldFail(element, wrapper) {
		var id = element.attr('id'),
				errorWrapper = wrapper,
		 		elementParent = element.closest('.lblFieldPairV, .lblFieldPair, tr'),
				elementError = $("#"+id+"-err");
		
		// Highlight field and show error
		elementParent.addClass('alertHighlight');
		elementError.removeClass('hidden');
		
		// Show Error Box
		$(errorWrapper).addClass('visible');
		errorBox($(errorWrapper));
	};

	function requiredFieldSuccess(element, wrapper) {
		var id =element.attr('id'),
				errorWrapper = wrapper,
			 	elementParent = element.closest('.lblFieldPairV, .lblFieldPair, tr'),
				elementError = $("#"+id+"-err");

		// Remove highlight from field and show error
		elementParent.removeClass('alertHighlight');
		elementError.addClass('hidden');
		
		// Hide Error Box
		errorBox($(errorWrapper));
	};

	function errorBox(elem) {
		if (elem.find('li:visible').length === 0) {
		 	elem.removeClass('visible');
		} else if (elem.find('li:visible').length != 0) {
			elem.addClass('visible');
		}
	};

	function phoneFormatCheck(value) {
    value = $.trim(value).replace(/\D/g, '');

    if (value.substring(0, 1) == '1') {
        value = value.substring(1);
    }

    if (value.length == 10) {
        return value;
    }
    return false;
	}

	// Incur Fees or Expenses
	$(document).on('click', 'input[name=feesExpenses]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'feesExpenses-yes') {
			$('.feesExpenses_wrapper').removeClass('hidden');
		} else {
			$('.feesExpenses_wrapper').addClass('hidden');
		}
	});

	// Minimum Distribution
	$(document).on('click', 'input[name=minDistReq]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'minDistReq-no') {
			$('.minDistReq_wrapper').removeClass('hidden');
		} else {
			$('.minDistReq_wrapper').addClass('hidden');
		}
	});

	// Process RMD now?
	$(document).on('click', 'input[name=processRMDNow]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'processRMDNow-no') {
			$('.processRMDNow_wrapper').removeClass('hidden');
		} else {
			$('.processRMDNow_wrapper').addClass('hidden');
		}
	});

	// Type of Transfer
	$(document).on('change', 'input[name=typeTransfer]', function() {
		var $id = $(this).attr('id');
		
		if ($id =='typeTransfer-60day') {
			$('.typeDirect').addClass('hidden');
			$('.type60Day').removeClass('hidden');
		} else {
			$('.typeDirect').removeClass('hidden');
			$('.type60Day').addClass('hidden');
		}
	});

	// TPA2Cash or Lump Sum
	$(document).on('click', 'input[name=typeDirectAssetTransfer]', function() {
		var $id = $(this).attr('id'),
				$wrapper = '.' + $id + '_wrapper';
					
		if ($(this).is(':checked')) {
			$($wrapper).removeClass('hidden');
		} else {
			$($wrapper).addClass('hidden');
		}
	});

	//Amount To Transfer
	$(document).on('click', 'input[name=transferEntireAmount]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'transferEntireAmount-yes') {
			$('.transferEntireAmount-Yes_wrapper').removeClass('hidden');
			$('.transferEntireAmount-No_wrapper').addClass('hidden');
		} else {
			$('.transferEntireAmount-No_wrapper').removeClass('hidden');
			$('.transferEntireAmount-Yes_wrapper').addClass('hidden');
		}
	});

	//Fed Withheld
	$(document).on('click', 'input[name=fedWithheld]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'fedWithheld-yes') {
			$('.fedWithheld-Yes_wrapper').removeClass('hidden');
		} else {
			$('.fedWithheld-Yes_wrapper').addClass('hidden');
		}
	});

	//Term Prior Employer
	$(document).on('click', 'input[name=termPriorEmployer]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'termPriorEmployer-yes') {
			$('.termPriorEmployer_wrapper').removeClass('hidden');
		} else {
			$('.termPriorEmployer_wrapper').addClass('hidden');
		}
	});

	//Term Prior Employer
	$(document).on('click', 'input[name=rothAccumulations]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'rothAccumulations-yes') {
			$('.handleRoth_wrapper').removeClass('hidden');
		} else {
			$('.handleRoth_wrapper').addClass('hidden');
		}
	});

	//Term Prior Employer
	$(document).on('click', 'input[name=newAllocations]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'newAllocations-different') {
			$('.selectDifferentAllocations_wrapper').removeClass('hidden');
		} else {
			$('.selectDifferentAllocations_wrapper').addClass('hidden');
		}
	});

	//Fed Withholding Amount
	$(document).on('click', 'input[name=fedWithholdingOption]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'fedWithholdingOption-Yes') {
			$('.howMuchFedWithholdingOption_wrapper').removeClass('hidden');
		} else {
			$('.howMuchFedWithholdingOption_wrapper').addClass('hidden');
		}
	});
	
	//Fed Withholding Amount
	$(document).on('change', '#howMuchFedWithholdingOption', function() {
		$val = $(this).val();
		
		if ($val == 'Percent') {
			$('#fedWithholdingAmount').attr('placeholder', '%').addClass('txtr');
		} else {
			$('#fedWithholdingAmount').attr('placeholder', '$').removeClass('txtr');
		}
	});

	// Date of Leav Heart Act
	$(document).on('click', 'input[name=heartAct]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'heartAct-Yes') {
			$('.dateofLeaveHeartAct_wrapper').removeClass('hidden');
		} else {
			$('.dateofLeaveHeartAct_wrapper').addClass('hidden');
		}
	});

	// Currently Disabled
	$(document).on('click', 'input[name=currentlyDisabled]', function() {
		$id = $(this).attr('id');
		
		if ($id == 'currentlyDisabled-Yes') {
			$('.dateCurrentlyDisabled_wrapper').removeClass('hidden');
		} else {
			$('.dateCurrentlyDisabled_wrapper').addClass('hidden');
		}
	});	

	//Lump Sum Options
	$(document).on('click', 'input[name=rolloverAmountLump]', function() {
		$id = $(this).attr('id');
		$('.rolloverWrapper').addClass('hidden');
		if ($id == 'rolloverAmountLump-Option1') {
			$('.rollover-Option1_wrapper').removeClass('hidden');	
		} else if ($id == 'rolloverAmountLump-Option2') {
			$('.rollover-Option2_wrapper').removeClass('hidden');
		} else if ($id == 'rolloverAmountLump-Option3') {
			$('.rollover-Option3_wrapper').removeClass('hidden');
		}
	});

	// CD Selection on Funding Details
	var $cdSelect = false;
	$(document).on('change', '#investmentTypeDetail', function(){
		var $val = $(this).val();
		if ($val == 'cd') {
			$cdSelect = true;
		} else {
			$cdSelect = false;
		}
		
		if ($cdSelect) {
			$('.nonCDContent').addClass('hidden');
			$('.CDContent').removeClass('hidden');
		}
	});
	
	
	$(document).on('change', '#transferType', function() {
		// Reset Authorization Requirements Form
		$('.tiaaSendForms_wrapper, .thirdPartyForms_wrapper, .thirdParty_wrapper, .carrierForms_wrapper, .timeUntilClientReceives_wrapper, .selectSignatory_wrapper, .medallionSignature_wrapper, .howFormsAccepted_wrapper, .sendMethod_wrapper, .returnMethod_wrapper, .howFundsSent_wrapper, .followUp_wrapper, .turnAround_wrapper, .sourceContactInfo_wrapper, .returnedFormsTiaa_wrapper').addClass('hidden');
		$('.authRequirementsForm').find("input[type=text], select").val("");
		$('.authRequirementsForm input[type="radio":checked]:not("input[name=custAuth]")').each(function(){
			$(this).prop('checked', false);
  	});
  	$('.authRequirementsForm input[name=custAuth]').prop('checked', false);
  	// Reset Forms Handling Summary Panel
  	formsHandlingSummaryUpdate('clientAuthReqIndicator', 'TBD');
		formsHandlingSummaryUpdate('reqFormsIndicator', 'TBD');
		formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'TBD');
		formsHandlingSummaryUpdate('delMethodCarierIndicator', 'TBD');
		formsHandlingSummaryUpdate('delMethodClientIndicator', 'TBD');

		var $val = $(this).val();
		authReqModel['transferTypeVal'] = $val;
		//console.log('Transfer type: ' + authReqModel['transferTypeVal']);
		if ($val.indexOf('directTransfer')) {			
			$('.accountTypeDirect').addClass('hidden');
			$('.accountTypeOther').removeClass('hidden');
		} else {
			$('.accountTypeDirect').removeClass('hidden');
			$('.accountTypeOther').addClass('hidden');
		}
	});

	// IGO/NIGO - Authorization Requirements
	$(document).on('change', 'input[name=custAuthRO]', function() {
		$('.authRequirementsROForm').addClass('hidden');
		$('.authRequirementsForm').removeClass('hidden');
	});
	// IGO/NIGO - Funding Forms Review
	$(document).on('change', 'input[name=formStatus]', function() {
		var $id = $(this).attr('id');
		if ($id === 'formStatus-nigo') {
			$('.igo_wrapper').addClass('hidden');
			$('.nigo_wrapper').removeClass('hidden');
		} else {
			$('.nigo_wrapper').addClass('hidden');
			$('.igo_wrapper').removeClass('hidden');
		}

		if ($id === 'formStatus-igoMsg') {
			$('.imagingTeamInstructions').text('Provide instructions for the Imaging Transfers Team to complete MSG Delivery and alternate carrier package prep.');
		} else {
			$('.imagingTeamInstructions').text('Provide instructions for the Imaging Transfers Team to complete alternate carrier package prep.');	
		}
	});
	// MSG IGO/NIGO - Funding Forms Review
	$(document).on('change', 'input[name=nigoFormStatus]', function() {
		var $id = $(this).attr('id');
		if ($id === 'nigoFormStatus-nigo') {
			$('.msgNigo_wrapper').removeClass('hidden');
			$('.msgIgo_wrapper').addClass('hidden');
		} else {
			$('.msgNigo_wrapper').addClass('hidden');
			$('.msgIgo_wrapper').removeClass('hidden');
		}
	});
	//MSG NIGO Error Check
	$(document).on('click', '.msgNigoSubmitBtn', function() {
		if ($('#msgNigoComment').val() == '') {
	    $('.msgNigoError').removeClass('hidden');
	    $('#msgNigoComment').prev().addClass('alertHighlight');
		} else {
	   	$('.msgNigoError').addClass('hidden');
	   	$('#msgNigoComment').prev().removeClass('alertHighlight');
	   	$('.msgIgo_wrapper').addClass('hidden');
	   	$('.nigoConfirmation').addClass('visible');

		}
	});
	// IGO/NIGO Show Other 
	$(document).on('click', 'input[name=otherNIGOReasons]', function() {
		if ($(this).is(':checked')) {
			$('.otherNIGOComment_wrapper').removeClass('hidden');
		} else {
			$('.otherNIGOComment_wrapper').addClass('hidden');
		}
	});

	// Show/Hide IGO/NIGO Submit
	$(document).on('click', '.nigoSubmitBtn', function() {
		//NIGO Error Check
		if ($('.nigo_wrapper input:checkbox:checked').length > 0) {
	    $('.nigoError').removeClass('visible');
	    $('.nigoSubmitted').removeClass('hidden');
			$('.nigo_wrapper').addClass('hidden');
			$('.nigoTSS').addClass('hidden');
			$('.igoSubmitted').addClass('hidden');
			$('.igo_wrapper').addClass('hidden');
			$('.nigoHeader').addClass('hidden');
		} else {
	   	$('.nigoError').addClass('visible');
		}
	});
	$(document).on('click', '.nigoEditBtn', function() {
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').removeClass('hidden');
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').addClass('hidden');
		$('.nigoHeader').removeClass('hidden');
	});
	$(document).on('click', '.igoSubmitBtn', function() {
		$('.igoSubmitted').removeClass('hidden');
		$('.igo_wrapper').addClass('hidden');
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.nigoHeader').addClass('hidden');
	});
	$(document).on('click', '.igoEditBtn', function() {
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').removeClass('hidden');
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.nigoHeader').removeClass('hidden');
	});
	$(document).on('click', '.hiddenRST', function() {
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').addClass('hidden');
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.nigoHeader').addClass('hidden');
		$('.nigoRST').removeClass('hidden');
	});
	$(document).on('click', '.hiddenTSS', function() {
		$('.igoSubmitted').addClass('hidden');
		$('.igo_wrapper').addClass('hidden');
		$('.nigoSubmitted').addClass('hidden');
		$('.nigo_wrapper').addClass('hidden');
		$('.nigoHeader').removeClass('hidden');
		$('.nigoTSSConfirm').addClass('hidden');
		$('.nigoTSS').removeClass('hidden');
		$('input[name=formStatus]').removeAttr('checked');
	});
	// IGO/NIGO - NIGO RST
	$(document).on('change', 'input[name=nextSteps]', function() {
		var $id = $(this).attr('id');
		$('.rst_wrapper').addClass('hidden');
		if ($id === 'nextSteps-resend') {
			$('.resend_wrapper').removeClass('hidden');
		} else if ($id === 'nextSteps-awaitingForms') {
			$('.awaitingForms_wrapper').removeClass('hidden');
		} else if ($id === 'nextSteps-awaitingDocs') {
			$('.awaitingDocs_wrapper').removeClass('hidden');
		} else if ($id === 'nextSteps-resolve') {
			$('.resolve_wrapper').removeClass('hidden');
		}
	});
	
	// IGO/NIGO - RST Confirm
	$(document).on('click', '.submitRSTBtn', function(){
		$('.nigoRST').addClass('hidden');
		$('.nigoRSTConfirm').removeClass('hidden');
	});

	// IGO/NIGO - TSS Confirm
	$(document).on('click', '.submitTSSBtn', function(){
		$('.nigoRST').addClass('hidden');
		$('.nigoTSSConfirm').removeClass('hidden');
	});


	// LOA Show Letter Link
	$(document).on('change', '#letterOfAcceptance', function() {
		$('.loaDoc_wrapper').removeClass('hidden');
	});

	// Save & Exit Check
	$(document).on('click', '#saveExitBtn', function() {
		if (!$('input[name=loaPrinted]').is(':checked')) {
			$('.loaAlertText').removeClass('hidden');
		} else {
			$('.loaAlertText').addClass('hidden');
		}
	});

	// Other Authorizations
	$(document).on('change', 'input[name=otherAuth]', function() {
		var $id = $(this).attr('id');

		if ($id === 'otherAuth-yes') {
			$('#otherInput').removeClass('hidden');
		} else {
			$('#otherInput').addClass('hidden');
		}
	});



	// Check that Maturity Date selected is not beyond 180 days
	$(document).on('change', '#maturityDate', function() {
			var $selectedDate = $(this).datepicker('getDate'),
					fullDate = new Date(),
					diffDate = ($selectedDate - fullDate)/1000/60/60/24;

			if (diffDate > 180) {
				$("#popupMaturity").dialog('open');
			}			
	});
	
	// Populate Bank Accoutn on File - SIM Code, NOT for DEV
	$(document).on('change', '#acctOnFile', function(){
		$('#acctType-checking').prop("checked", true);
		$('#acctHolderFName').val('Penelope');
		$('#acctHolderMName').val('');
		$('#acctHolderLName').val('Pension');
		$('#bankRoutingNumber').val('000000186');
		$('#bankAccountNumber').val('000000529');
	});


	// Auto opening/closing panels for Funding Edit Details
	$(document).on('click', '.transferDetailsPanelBtn', function() {
		var $trDetailPanel 	= $(document).find('a#transferDetailsPanel'),
			$hd 			= $trDetailPanel.closest('.hd');
		$trDetailPanel.trigger('click');
		$hd.find('#indicator1').text('Complete');
		$hd.find('.indicatorRed').removeClass('indicatorRed flr').addClass('indicatorGreen flr');
		//$(document).find('a#authRequirementsPanel').trigger('click');
	});
	$(document).on('click', '.authRequirementsPanelBtn', function() {
		$(document).find('a#authRequirementsPanel').trigger('click');
		$(document).find('a#handlingInstructionsPanel').trigger('click');
	});


	$(document).on('click', '#continueDetailBtn', function(e) {
		e.stopPropagation();
		errorWrapper = ('#' + $('#tDetailsAlerts').attr('id'));
		$(".transferDetailWrapper").find('[data-required=true]').each(function() {
			var element = $(this),
					elementVal = $.trim(element.val());
			
			if (element.is(':hidden')) { 
				requiredFieldSuccess(element, errorWrapper);
			} else {
				if (element.is(':checkbox') || element.is(':radio')) { //If Radio or Checkbox
					if (!$(element).is(":checked")) {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}
				} else { // All other inputs
					if (elementVal == 0 || typeof elementVal == "undefined" || elementVal == "") {
						requiredFieldFail(element, errorWrapper);
					} else {
						requiredFieldSuccess(element, errorWrapper);
					}	
				}
			}
		});
		return false;
	});

	// $(document).on('click', '#confirmDetailBtn', function(e) {
		// e.stopPropagation();
		// errorWrapper = ('#' + $('#sourceDetailErrors').attr('id'));
		// $("#inputDetails").find('[data-required=true]').each(function() {
			// var element = $(this),
					// elementVal = $.trim(element.val());
			
			// if (element.is(':hidden')) { 
				// requiredFieldSuccess(element, errorWrapper);
			// } else {
				// if (element.is(':checkbox') || element.is(':radio')) { //If Radio or Checkbox
					// if (!$(element).is(":checked")) {
						// requiredFieldFail(element, errorWrapper);
					// } else {
						// requiredFieldSuccess(element, errorWrapper);
					// }
				// } else { // All other inputs
					// if (elementVal == 0 || typeof elementVal == "undefined" || elementVal == "") {
						// requiredFieldFail(element, errorWrapper);
					// } else {
						// requiredFieldSuccess(element, errorWrapper);
					// }	
				// }
			// }
		// });
		// return false;
	// });
	// END - Forms Validation for Transfer Details

	
	$(document).on('click','input[name=wtradio]',function(){
		if($('#wtradiono').is(':checked')) { 
		 	$(".wtradionoDiv").removeClass('hidden');
		}
		if($('#wtradioyes').is(':checked')) { 
		 	$(".wtradionoDiv").addClass('hidden');
		}
	});
	$(document).on('click','input[name=allocationChoice]',function(){
		if($('#allocationChoiceNo').is(':checked')) { 
		 	$(".contributionsMadeSoFar").removeClass('hidden');
		} else {
		 	$(".contributionsMadeSoFar").addClass('hidden');
		}
	});
	


	// Show/hide for Create New funding Request
	$(".createNewFundingRequest").click(function() {
	    $(".createNewFundingRequest").addClass("hidden");
	    $(".accountToFund-wrapper").removeClass("hidden");
	}); 

	// Show/Hide Funding Sources for Fund an Account
	var $origVal = '';
	var $newVal = '';
	$('.addFundingToConfirmBtn').on('click', function() {
		var $val = $("#addFundingTo").val();
		if ($val != "") {
			$('.panels').removeClass("hidden");
			$(".alertText").addClass("hidden");
			$("#addFundingTo").parent().removeClass("alertHighlight");
			$(".addFundingToConfirmBtn").addClass("hidden");
			$("#addFundingTo option[value='']").remove();
			$origVal = $("#addFundingTo").val();
			$("#addFundingTo").attr("id", "addFundingToChange");
			$('.orchestrationId_wrapper').removeClass('hidden');
		} else {
			$('.panels').addClass("hidden");
			$(".alertText").removeClass("hidden");
			$("#addFundingTo").parent().addClass("alertHighlight");
		}
	});

	$(document).on('change', '#addFundingToChange', function(){
		$newVal = $("#addFundingToChange").val();
		if ($newVal != $origVal) {
			$('#eraseRequest').dialog('open');
		}
	});
	$('#yesChangeBtn').on('click', function() {
		$origVal = $("#addFundingToChange").val();
		$('#eraseRequest').dialog('close');
	});
	$('#noChangeBtn').on('click', function() {
		$("#addFundingToChange").val($origVal);
		$('#eraseRequest').dialog('close');
	});

	// Show Bank Info
	$(document).on('change', 'input[name="eftProvide"]', function() {
	  var $id = $(this).attr('id');
		if($id == 'eftProvide-phone') {
			$('.bankInfo_wrapper').removeClass('hidden');
	  } else {
	  	$('.bankInfo_wrapper').addClass('hidden');
	  }
	});

	// Show hide External rollovers
	$("input[name='external-rollovers']").click(function() {
		var $id 			= $(this).attr("id"),
			$addExtSrcPopup = $('#addExtSrcPopup');
		if($id == "external-rollovers-yes") {
			$('.extSources-wrapper').removeClass('hidden');
			$addExtSrcPopup.dialog('open');
		} else {
			$('.extSources-wrapper').addClass('hidden');
			$addExtSrcPopup.dialog('close');
		}
	});
	
	// Add button functionality from 'Add External Source Popup
	$(document).on('click','#addExtSrcBtn',function(e){
		e.preventDefault();
		window.open('edit_funding_detail.html','_self')
	});
	// Show hide External rollovers
	$("input[name='internal-rollovers']").click(function() {
	  var $id = $(this).attr("id");
		if($id == "internal-rollovers-yes") {
			$('.intSources-wrapper').removeClass('hidden');
	  } else {
	  	$('.intSources-wrapper').addClass('hidden');
	  }
	});

	// Search Type, on change populate Business Units Dropdown
  $('#companyName').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('.otherCompany-wrapper').removeClass('hidden');
    } else {
    	$('.otherCompany-wrapper').addClass('hidden');
    }
  });

  // Search Type, on change populate Business Units Dropdown
  $('#companyNameDetail').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('#otherCompany').removeClass('hidden');
    	$('#companyNameDetail').removeClass('inputxlg').addClass('inputs');
    } else {
    	$('#otherCompany').addClass('hidden');
    	$('#companyNameDetail').addClass('inputxlg').removeClass('inputs');
    }
  });

  // Search Type, on change populate Business Units Dropdown
  $('#productNameDetail').on('change', function() {
    var $val = $(this).val();
    if ($val == 'Other') {
    	$('#otherProduct').removeClass('hidden');
    	$('#productNameDetail').removeClass('inputxlg').addClass('inputs');
    } else {
    	$('#otherProduct').addClass('hidden');
    	$('#productNameDetail').addClass('inputxlg').removeClass('inputs');
    }
  });

	
	// Add External Sources to Table
	$(document).on('click', '#addBtn', function(e) {
		var $companyName = $('#companyName').val(),
				$otherCompany = $('#otherCompany').val(),
				$company = ($companyName == "Other") ? $otherCompany : $companyName,
				$transferAmount = $('#transferAmount').val(),
				row = '<tr>' +
								'<td>' + $company + '</td>' +
                '<td>' + $transferAmount + '</td>' +
                '<td></td>' +
                '<td></td>' +
                '<td>92Z1776H21</td>' +
                '<td class="status"><span class="down">Incomplete</span></td>' +
                '<td>' +
                  '<a href="edit_funding_detail.html" class="editBtn">Edit</a>' +
                  '<span class="dim pls prs">|</span>' +
                  '<a href="#cancelExtSourcePopup" class="cancelBtn">Remove</a>' +
                '</td>' +
              '</tr>';
		// Initialize Table & Form
    $('.noDataToDisplay').remove();
    // Add collected data
    $('#tbl_extsources0 tbody').append(row);
    // Close dialog
    $('#addExternalsourcePopup').dialog('close');
    // Clear Form for next use
    $(this).closest('form').find("input[type=text], select").val("");
  });

  

// // Begin Add/Remove Rows for Beneficiaries

//   // Remove External Sources from table
//   var $rowToRemove = '';
//   $(document).on('click', '.cancelBtn', function(e){
//   	//Show Dialog
//   	$('#cancelExtSourcePopup').dialog('open');
//   	// Set var to which row being removed
//   	$rowToRemove = $(this).closest('tr');
//   });


// 	$(document).on('click', '#cancelSourceBtn', function(e) {
// 		// Remove row designated by var
// 		$rowToRemove.remove();
// 		// Default placeholder row
// 		var row = '<tr>' +
//               	'<td colspan="7" class="noDataToDisplay">No data to display</td>' +
//               '</tr>';
//     // If table is empty, replace with placeholder row
//     if ($('#tbl_extsources0 tbody tr').children().length <= 1) {
//     	$('#tbl_extsources0 tbody').append(row);
//     }
//     //Close dialog 
//     $('#cancelExtSourcePopup').dialog('close');
//     // Re-Init var
//     $rowToRemove = '';
//   });
// // Ended Add/Remove Rows for Beneficiaries
  




  // Set External Sources to Canceled
  var $rowTochange = '';
  $(document).on('click', '.cancelBtn', function(e){
  	//Show Dialog
  	$('#cancelExtSourcePopup').dialog('open');
  	// Set var to which row being removed
  	$rowTochange = $(this).closest('tr');
  });
	$(document).on('click', '#cancelSourceBtn', function(e) {
		// Change Row status
		$rowTochange.find('td.status span').text('Removed').removeClass('down');    
    //Close dialog 
    $('#cancelExtSourcePopup').dialog('close');
    // Re-Init var
    $rowTochange = '';
  });
  
	//Display Error for validation
	function showError($id){	
		$('#sourceDetailErrors').addClass('visible');
		$('#'+$id+'-err').removeClass('closed');	
	}
	
	
	
	// Confirm/Update button
	$(document).on('click', '.editReadMode', function(e){
		e.preventDefault();
		//Validation for Source Detail Section
		$('#inputDetails').find('[data-required="true"]').each(function(){
			var $id   = $(this).attr('id');
			console.log($id+': '+$(this).val());	
			if($(this).val() === '' ){			
				showError($id);
			}else if($(this).val() === 'Other'){
				if($(this).next('input').val() == ''){
					showError($id);				
				}else{
					$('#'+$id+'-err').addClass('closed');
				}		
			}else{
				$('#'+$id+'-err').addClass('closed');
			}			
		
		});
		if ($('#sourceDetailErrors').find('li:visible').length === 0) {			
			$('#sourceDetailErrors').removeClass('visible');
			$('.transferDetailWrapper').removeClass('hidden');
			if ($('#inputDetails').hasClass('hidden')) {
				$('#inputDetails').removeClass('hidden');
				$('#inputReadOnly').addClass('hidden');
			} else {
				$('#inputDetails').addClass('hidden');
				$('#inputReadOnly').removeClass('hidden');
				$('#addFundBtn').removeClass('btnOff');
				if (GetURLParameter("address")=="true"){
					$adrSrh.add($authReqPanel).add($handleInsPanel).removeClass('closed');
					$nonadrSrh.addClass('closed');
					$('.nonCDContent, .CDContent').toggleClass('closed');
					if($('#companyNameDetail').find('option:selected').val() === 'Other'){
						$('.compNameRead').text($('#otherCompany').val());
						$('.otherAdrLink').removeClass('closed');
					}else{
						$('.compNameRead').text($('#companyNameDetail').find('option:selected').val());
						$('.otherAdrLink').addClass('closed');
					}
					
				}else{
					$adrSrh.add($authReqPanel).add($handleInsPanel).addClass('closed');
					$nonadrSrh.removeClass('closed');
				}	
			}
		}
	});
	
	//Search Look up
	
	$(document).on('click','.adrLookUp',function(e){
		e.preventDefault();
		$('#adrCarrName').val($('.compNameRead').text());
		$('#srhLookPopup').dialog('open');	
	});
	
	$(document).on('click','#searchBtn',function(e){
		e.preventDefault();
		$('#srhLookForm').find('input').each(function(){
			if($(this).val() == ''){
				$('#srhLookErrors').addClass('visible');
				$('#'+$(this).attr('id')+ '-err').removeClass('closed');
			}else{
				$('#'+$(this).attr('id')+ '-err').addClass('closed');
			}		
		});
		if ($('#srhLookErrors').find('li:visible').length === 0) {	
			$('#srhLookErrors').removeClass('visible');
			$('.srhResultsDiv').removeClass('closed');
			$('.adrCarrName-Txt').html($('#adrCarrName').val());
			$('.adrZip-Txt').html($('#adrZip').val());
		}
		
	});

	// Dollar formatting
	$(document).on('blur', '.dollarFormat', function() {	
		var $this   = this.value;
			$this	= parseFloat(this.value.replace(/,/g, ""))
				.toFixed(2)
				.toString()
				.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		($this != 'NaN')?	this.value = '$' + $this : this.value = '$1,000.00';  
	});
  
  // Percentage formatting
  $(document).on('blur', '.percentageFormat', function() {
  	if ($(this).val().indexOf("%") > -1 ) {
    	this.value = this.value.substr(0,this.value.length-1) + '%';
   	} else {
   		this.value = this.value + '%';
   	}
  });


	// Phone/Digital/Wet Signature - First choice
  $(document).on('change', 'input[name=custAuth]', function() {
		//Reset Form
		$('.tiaaSendForms_wrapper, .thirdPartyForms_wrapper, .thirdParty_wrapper, .carrierForms_wrapper, .timeUntilClientReceives_wrapper, .selectSignatory_wrapper, .medallionSignature_wrapper, .howFormsAccepted_wrapper, .sendMethod_wrapper, .returnMethod_wrapper, .howFundsSent_wrapper, .followUp_wrapper, .turnAround_wrapper, .sourceContactInfo_wrapper').addClass('hidden');
		$('.authRequirementsForm').find("input[type=text], select").val("");
		$('.authRequirementsForm input[type="radio":checked]:not("input[name=custAuth]")').each(function(){
      $(this).prop('checked', false);
  	});
		// Reset Forms Handling Summary Panel
  	formsHandlingSummaryUpdate('clientAuthReqIndicator', 'TBD');
		formsHandlingSummaryUpdate('reqFormsIndicator', 'TBD');
		formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'TBD');
		formsHandlingSummaryUpdate('delMethodCarierIndicator', 'TBD');
		formsHandlingSummaryUpdate('delMethodClientIndicator', 'TBD');

		var $id = $(this).attr('id');
		authReqModel['custAuthMethodVal'] = $id;
		
		if($id == 'custAuth-phone') {
			$('.howFundsSent_wrapper').removeClass('hidden');
		} else {
			$('.digitalAuth').addClass('hidden');
		}

		if($id == 'custAuth-digital' || $id == 'custAuth-wetSig' ) {
			$('.carrierForms_wrapper').removeClass('hidden');
		} else {
			$('.carrierForms_wrapper').addClass('hidden');
		}
		if($id == 'custAuth-digital') {
			formsHandlingSummaryUpdate('clientAuthReqIndicator', 'DocuSign');			
		} else if($id == 'custAuth-wetSig') {
			formsHandlingSummaryUpdate('clientAuthReqIndicator', 'Wet Sign');
		} else if($id == 'custAuth-phone') {
			formsHandlingSummaryUpdate('clientAuthReqIndicator', 'Phone');
			formsHandlingSummaryUpdate('reqFormsIndicator', 'NA');
			formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'No MSG');
			formsHandlingSummaryUpdate('delMethodCarierIndicator', 'NA');
			formsHandlingSummaryUpdate('delMethodClientIndicator', 'NA');
		}

	});

  	// Digitial or Wet Signature path
  	// Carrier Forms
  	$(document).on('change', 'input[name=carrierForms]', function(){
			var $id = $(this).attr('id'),
					$transferType = authReqModel['transferTypeVal'];

			if($id != '') {
				// Direct Rollover
				if ($transferType.indexOf('directRollover') > -1) {
					$('.thirdParty_wrapper').removeClass('hidden');
				} else {
					$('.thirdParty_wrapper').addClass('hidden');
				}				
				// Direct Transfer
				if($id != 'carrierForms-tiaa') {
					$('.timeUntilClientReceives_wrapper').removeClass('hidden');
				} else {
					$('.timeUntilClientReceives_wrapper').addClass('hidden');
				}
				$('.medallionSignature_wrapper').removeClass('hidden');
			} else {
				$('.medallionSignature_wrapper').addClass('hidden');
				$('.timeUntilClientReceives_wrapper').addClass('hidden');
			}

			// Update Forms Handling Summary Panel
			if($id == 'carrierForms-tiaa') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'TIAA');
			} else if($id == 'carrierForms-alt') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'Alternate Carrier');
			} else if($id == 'carrierForms-both') {
				formsHandlingSummaryUpdate('reqFormsIndicator', 'Alt Carrier & TIAA');
			}
		});	

  	// Third Party Forms Required
  	$(document).on('change', 'input[name=thirdParty]', function(){
			var $id = $(this).attr('id');
			if($id == 'thirdParty-yes') {
				$('.thirdPartyForms_wrapper').removeClass('hidden');
			} else {
				$('.thirdPartyForms_wrapper').addClass('hidden');
			}
		});
		// Medallion		
		
  	$(document).on('change', 'input[name=medallionSignature]', function(){
		var $id = $(this).attr('id');
		if($id != '') { console.log($('#transferType option:selected').val());
			$('input[name=tiaaSendForms]').removeAttr('checked');			
			if($id == 'medallionSignature-loa') {
				if($('#transferType option:selected').val() == 'directRolloverCash'){
					$('.howFormsAccepted_wrapper .wetSignShow2').addClass('closed');
				}else{
					$('.howFormsAccepted_wrapper .wetSignShow2').removeClass('closed');
				}
				$('#tiaaSendForms-edelivery').closest('li').addClass('closed');
				$('.selectSignatory_wrapper').removeClass('hidden');
				$('.returnedFormsTiaa_wrapper').addClass('hidden');
			} else {
				if($id == 'medallionSignature-yes'){ 
					if( $('#transferType option:selected').val() == 'directRolloverCash'){
						$('.howFormsAccepted_wrapper .wetSignShow2').removeClass('closed');
					}else{
						$('.howFormsAccepted_wrapper .wetSignShow2').removeClass('closed');
					}
				}
				if($id == 'medallionSignature-no') { 
					if( $('#transferType option:selected').val() == 'directRolloverCash'){
						$('.howFormsAccepted_wrapper .wetSignShow2').addClass('closed')
					}else{
						$('.howFormsAccepted_wrapper .wetSignShow2').removeClass('closed');
					}
					$('.returnedFormsTiaa_wrapper').addClass('hidden');
				}
				$('#tiaaSendForms-edelivery').closest('li').removeClass('closed');
				$('.selectSignatory_wrapper').addClass('hidden');
			}
			if($id == 'medallionSignature-yes') {
				$('.digitalAuth').addClass('hidden');
			}else{
				$('.digitalAuth').removeClass('hidden');
			}
			if(authReqModel['custAuthMethodVal'] == 'custAuth-wetSig') {
				$('.howFormsAccepted_wrapper .docuSignShow, .wetSignShow2').removeClass('hidden');
				
				$('.howFormsAccepted_wrapper .wetSignShow').addClass('hidden');
			} else {
				$('.howFormsAccepted_wrapper .docuSignShow, .wetSignShow2').addClass('hidden');
				$('.howFormsAccepted_wrapper .wetSignShow').removeClass('hidden');
			}
			$('.howFormsAccepted_wrapper').removeClass('hidden');
		} else {
			$('.howFormsAccepted_wrapper').addClass('hidden');
			$('.selectSignatory_wrapper').addClass('hidden');
		}
		// Update Forms Handling Summary Panel
		if($id == 'medallionSignature-yes') {
			formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'MSG Required');
		} else if($id == 'medallionSignature-no') {
			formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'No MSG');
		} else if($id == 'medallionSignature-loa') {
			formsHandlingSummaryUpdate('tiaaAuthReqIndicator', 'No MSG, but Wet LOA');
		}
	});

	

		// How Forms are Accepted
  	$(document).on('change', 'input[name=formsAccepted]', function(){
			var $id 	  = $(this).attr('id'),
				$custAuth = $('input[name=custAuth]:checked').attr('id');			
			
			if ($custAuth == 'custAuth-digital') {
				// Go to 'How will the funds be sent?'
				if($id != '') {
					$('.howFundsSent_wrapper').removeClass('hidden');
				} else {
					$('.howFundsSent_wrapper').addClass('hidden');
				}
			} else {
				// Go to 'How should TIAA send forms to the client?' 
				
				if($id != '') {	
					if($id == 'formsAccepted-original' || $id == 'formsAccepted-copy2' || $id == 'formsAccepted-fax'){
						($id == 'formsAccepted-copy2' || $id == 'formsAccepted-fax') ? $('.returnedFormCopyFax').removeClass('closed') : $('.returnedFormCopyFax').addClass('closed') ;
						
						$('.howFundsSent_wrapper').removeClass('hidden');
						$('.sendMethod_wrapper').addClass('hidden');	
					}else{
						$('.sendMethod_wrapper').removeClass('hidden');	
						$('.howFundsSent_wrapper').addClass('hidden');
					}
				} else {
					$('.sendMethod_wrapper').addClass('hidden');					
				}
			}
			// Update Forms Handling Summary Panel
			if($id == 'formsAccepted-original') {
				formsHandlingSummaryUpdate('delMethodCarierIndicator', 'Original Wet Signed Form');
			} else if($id == 'formsAccepted-copy') {
				formsHandlingSummaryUpdate('delMethodCarierIndicator', 'Copy Paper via Mail');
			} else if($id == 'formsAccepted-efax') {
				formsHandlingSummaryUpdate('delMethodCarierIndicator', 'eFax');
			}else if($id  == 'formsAccepted-copy2'){
				formsHandlingSummaryUpdate('delMethodCarierIndicator', 'Copy');
			}else if($id == 'formsAccepted-fax'){
				formsHandlingSummaryUpdate('delMethodCarierIndicator', 'Fax');
			}
		});
		// How Forms are Sent
  	$(document).on('change', 'input[name=sendMethod]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.howFundsSent_wrapper').removeClass('hidden');
			} else {
				$('.howFundsSent_wrapper').addClass('hidden');
			}
		});

		// Return Method
  // 	$(document).on('change', 'input[name=returnMethod]', function(){
		// 	var $id = $(this).attr('id');
		// 	if($id != '') {
		// 		$('.howFundsSent_wrapper').removeClass('hidden');
		// 	} else {
		// 		$('.howFundsSent_wrapper').addClass('hidden');
		// 	}
		// });



		// Phone Path
		// Funds Sent 
	  $(document).on('change', 'input[name=fundsSent]', function(){
			var $id = $(this).attr('id');
			if($id != '') {
				$('.turnAround_wrapper').removeClass('hidden');
			} else {
				$('.turnAround_wrapper').addClass('hidden');
			}
		});

	  // Follow Up
		// $(document).on('change', 'input[name=followUp]', function(){
	 	$(document).on('keypress keydown keyup change blur', 'input[name=turnAround]', function() {
			var $id = $(this).attr('id');
			if($id != '') {
				$('.followUp_wrapper').removeClass('hidden');
			} else {
				$('.followUp_wrapper').addClass('hidden');
			}
		});
		
		
		
	 	// Client Delivery Option
		$(document).on('change', 'input[name=followUp]', function() {
			var $id = $(this).attr('id');				
			if($id != '') {					
				$('.tiaaSendForms_wrapper').removeClass('hidden');
			} else {
				$('.tiaaSendForms_wrapper').addClass('hidden');
			}	
		});

		// Forms Returned to TIAA
		$(document).on('change', 'input[name=tiaaSendForms]', function() {
			var $id = $(this).attr('id'),
				$msgID 	    = $('input[name=medallionSignature]:checked').attr('id'),
				$custAuthID = $('input[name=custAuth]:checked').attr('id');				
				if(($custAuthID == 'custAuth-wetSig')){
					if($msgID == 'medallionSignature-yes'){
						$('.returnedFormsTiaa_wrapper').removeClass('hidden');
					}else{						
						$('.returnedFormsTiaa_wrapper').addClass('hidden');
					}
				}
			// if($id == 'tiaaSendForms-docusign') {
				// $('.returnedFormsTiaa_wrapper').removeClass('hidden');
			// } else {
				// $('.returnedFormsTiaa_wrapper').addClass('hidden');
			// }
			// Update Forms Handling Summary Panel
			if($id == 'tiaaSendForms-docusign') {
				formsHandlingSummaryUpdate('delMethodClientIndicator', 'DocuSign');
			} else if($id == 'tiaaSendForms-edelivery') {
				formsHandlingSummaryUpdate('delMethodClientIndicator', 'eDelivery');
			} else if($id == 'tiaaSendForms-paper') {
				formsHandlingSummaryUpdate('delMethodClientIndicator', 'Paper Mail (Print Local)');
			}
		});
		
		// Source Contact Information
	 //  $(document).on('keypress keydown keyup change focus blur', 'input[name=turnAround]', function() {
		// 	var $val = $(this).val(),
		// 			$custAuth = $('input[name=custAuth]:checked').attr('id');

		// 	if($val != '') { 
		// 		if ($custAuth != 'custAuth-phone') {
		// 			$('.sourceContactInfo_wrapper').removeClass('hidden');
		// 		} else {
		// 			$('.sourceContactInfo_wrapper').addClass('hidden');
		// 		}
		// 	}
		// });


	// Forms Accepted
	$(document).on('change', 'input[name=formsAccepted]', function() {
		var $id = $(this).attr('id');
		if ($id == 'formsAccepted-efax') {
			$('.efaxNumber').removeClass('hidden');
		} else {
			$('.efaxNumber').addClass('hidden');
		}

		// if ($id == 'formsAccepted-efax' || $id == 'formsAccepted-copy') {
		// 	$('.returnMethod_wrapper').removeClass('hidden');
		// } else {
		// 	$('.returnMethod_wrapper').addClass('hidden');
		// }
	});
	
	$(document).on('change', 'input[name=contributionType]', function(){
		$id = $(this).attr('id');
		if($id == 'contributionType-yes') {
			$('.eftOrCheck_wrapper').removeClass('hidden');
		}else{
			$('.eftOrCheck_wrapper').addClass('hidden');
		}
	});

	$(document).on('change', 'input[name=eftOrCheck]', function(){
		var $id = $(this).attr('id');

		if ($('input[name="eftOrCheck"]:checked').length > 0) {
			$('.contributionsMadeSoFar').removeClass('hidden');
		} else {
			$('.contributionsMadeSoFar').addClass('hidden');
		}
		
		if($(this).is(':checked')) {	
			if($id == 'eftOrCheck-eft') {
				$('.contributionEFT-wrapper').removeClass('hidden');
			}
			if($id == 'eftOrCheck-check') {
				$('.contributionCheck-wrapper').removeClass('hidden');
			}
			
		} else {
			if($id == 'eftOrCheck-eft') {
				$('.contributionEFT-wrapper').addClass('hidden');
			}
			if($id == 'eftOrCheck-check') {
				$('.contributionCheck-wrapper').addClass('hidden');
			}
		}
	});

	$(document).on('change', 'input[name=jointAccount]', function(){
		$id = $(this).attr('id');
		if($id == 'jointAccount-yes') {
			$('.joint_wrapper').removeClass('hidden');
			$('.bankName_wrapper').css('marginTop', '0px');
		}else{
			$('.joint_wrapper').addClass('hidden');
			$('.bankName_wrapper').css('marginTop', '17px');
		}
	});
	

	$(document).on('change', 'input[id=eftType-oneTime]', function(){
		if($(this).is(':checked')) {
			$('.oneTime_wrapper').removeClass('hidden');
		} else {
			$('.oneTime_wrapper').addClass('hidden');
		}
	});
	$(document).on('change', 'input[id=eftType-recurring]', function() {
		if($(this).is(':checked')) {
			$('.recurring_wrapper').removeClass('hidden');
			$('.totalFutureContributions_wrapper').removeClass('hidden');
		} else {
			$('.recurring_wrapper').addClass('hidden');
			$('.totalFutureContributions_wrapper').addClass('hidden');
		}
	});
	
	$(document).on('change', 'input[id=eftType-oneTime], input[id=eftType-recurring]', function(){
		if($(this).is(':checked')) {
			$('.totalContributions_wrapper').removeClass('hidden');
		} else {
			$('.totalContributions_wrapper').addClass('hidden');
		}
	});

	$(document).on('change', 'input[name=eftType]', function() {
		var $atLeastOneIsChecked = $('input[name="eftType"]:checked').length > 0;		
		if($atLeastOneIsChecked) {	
			$('.bankInformation').removeClass('hidden');
		}	else {
			$('.bankInformation').addClass('hidden');
		}
	});
	
	$(document).on('click focus blur change keyup', 'input[name=reccuringStart], #recurringMonthly', function() {
		var $monthly = $('#recurringMonthly').val(),
				$recurringDate = $(this).val();

  	if ($monthly != '' &&  $recurringDate != '') {
    	$('.recurringMonthly_wrapper').removeClass('hidden');
   	} else {
   		$('.recurringMonthly_wrapper').addClass('hidden');
   	}
  });


});